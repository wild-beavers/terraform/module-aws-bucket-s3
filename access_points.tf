#####
# Variables
#####

variable "access_points" {
  description = <<-DOCUMENTATION
    To manage an S3 Access Points.
    Keys are free values.

    - name                                     (optional, string): Name of the access point.
    - account_id                               (optional, string): AWS account ID for the owner of the bucket for which you want to create an access point. Defaults to automatically determined account ID of the Terraform AWS provider.
    - bucket_account_id                        (optional, string): AWS account ID associated with the S3 bucket associated with this access point.
    - policy                                   (optional, string): Valid JSON document that specifies the policy that you want to apply to this access point.
    - vpc_configuration_vpc_id                 (optional, string): Restrict access to this access point to requests from the specified Virtual Private Cloud (VPC). If not set, the access point will be considered public.
    - public_configuration_block_public_acls   (optional, bool): Whether Amazon S3 should block public ACLs for this access point. Defaults to `true`. Enabling this setting does not affect existing policies or ACLs. When set to `true` causes the following behavior:
        PUT Bucket acl and PUT Object acl calls fail if the specified ACL is public.
        PUT Object calls fail if the request includes a public ACL.
        PUT Bucket calls fail if the request includes a public ACL.
    - public_configuration_block_public_policy     (optional, bool): Whether Amazon S3 should block public bucket policies for this access point. Defaults to `true`. Enabling this setting does not affect existing bucket policies. When set to `true` causes Amazon S3 to: Reject calls to PUT Bucket policy if the specified bucket policy allows public access.
    - public_configuration_ignore_public_acls      (optional, bool): Whether Amazon S3 should ignore public ACLs for this access point. Defaults to `true`. Enabling this setting does not affect the persistence of any existing ACLs and doesn't prevent new public ACLs from being set. When set to `true` causes Amazon S3 to: Ignore all public ACLs on buckets in this account and any objects that they contain.
    - public_configuration_restrict_public_buckets (optional, bool): Whether Amazon S3 should restrict public bucket policies for this access point. Defaults to `true`. Enabling this setting does not affect previously stored bucket policies, except that public and cross-account access within any public bucket policy, including non-public delegation to specific accounts, is blocked. When set to `true`: Only the bucket owner and AWS Services can access buckets with public policies.
DOCUMENTATION
  type = map(map(object({
    name                                         = optional(string)
    account_id                                   = optional(string)
    bucket_account_id                            = optional(string)
    policy                                       = optional(string)
    vpc_configuration_vpc_id                     = optional(string)
    public_configuration_block_public_acls       = optional(bool, true)
    public_configuration_block_public_policy     = optional(bool, true)
    public_configuration_ignore_public_acls      = optional(bool, true)
    public_configuration_restrict_public_buckets = optional(bool, true)
  })))
  default  = {}
  nullable = false
}

#####
# Locals
#####

locals {
  buckets_with_access_points = { for key, bucket in var.buckets :
    key => merge(bucket, { access_points = lookup(var.access_points, key, null) }) if contains(keys(var.access_points), key)
  }

  access_points = merge([for key, bucket in local.buckets_with_access_points :
    { for access_point_key, access_point in bucket.access_points :
      "${key}_${access_point_key}" => merge(access_point, { bucket_id = aws_s3_bucket.this[key].id, name = coalesce(access_point.name, access_point_key) })
    }
  ]...)
}

#####
# Resources
#####

resource "aws_s3_access_point" "this" {
  for_each = local.access_points

  bucket            = each.value.bucket_id
  name              = "${var.testing_prefix}${each.value.name}"
  account_id        = each.value.account_id
  bucket_account_id = each.value.bucket_account_id

  dynamic "vpc_configuration" {
    for_each = each.value.vpc_configuration_vpc_id != null ? { 0 = 0 } : {}

    content {
      vpc_id = each.value.vpc_configuration_vpc_id
    }
  }

  dynamic "public_access_block_configuration" {
    for_each = each.value.vpc_configuration_vpc_id == null ? { 0 = 0 } : {}

    content {
      block_public_acls       = each.value.public_configuration_block_public_acls
      block_public_policy     = each.value.public_configuration_block_public_policy
      ignore_public_acls      = each.value.public_configuration_ignore_public_acls
      restrict_public_buckets = each.value.public_configuration_restrict_public_buckets
    }
  }
}

resource "aws_s3control_access_point_policy" "this" {
  for_each = { for k, v in local.access_points : k => v if v.policy != null }

  access_point_arn = aws_s3_access_point.this[each.key]
  policy           = each.value.policy
}

#####
# Outputs
#####

output "aws_s3_access_point" {
  value = length(local.access_points) > 0 ? {
    for key, configuration in aws_s3_access_point.this : key => {
      for attribute, value in configuration : attribute => value if !(contains(["public_access_block_configuration", "vpc_configuration", "policy"], attribute))
    }
  } : null
}
