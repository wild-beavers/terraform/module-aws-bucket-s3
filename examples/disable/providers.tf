provider "aws" {
  region     = "us-east-1"
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
  profile    = var.aws_profile

  assume_role {
    role_arn = var.aws_assume_role
  }
}

provider "aws" {
  region     = "us-west-1"
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
  profile    = var.aws_profile

  assume_role {
    role_arn = var.aws_assume_role
  }

  alias = "replica"
}
