#####
# Disabled
#####

module "disable" {
  source = "../../"

  count = 0

  providers = {
    aws.replica = aws.replica
  }
}
