###
# Minimal
###

data "aws_s3_bucket" "minimal" {
  bucket = module.minimal.aws_s3_buckets.minimal.id

  depends_on = [
    module.minimal
  ]
}

data "aws_s3_bucket_policy" "minimal" {
  bucket = module.minimal.aws_s3_buckets.minimal.id

  depends_on = [
    module.minimal
  ]
}

check "minimal" {
  assert {
    condition = (
      module.minimal.aws_s3_buckets.minimal.arn == data.aws_s3_bucket.minimal.arn &&
      startswith(module.minimal.aws_s3_buckets.minimal.id, local.prefix) &&
      module.minimal.aws_s3_access_point == null &&
      module.minimal.aws_iam_policies == null &&
      module.minimal.aws_kms_alias == null &&
      module.minimal.aws_kms_grants == null &&
      module.minimal.aws_kms_key == null &&
      module.minimal.aws_s3_bucket_website_configurations == null &&
      module.minimal.kms_aws_iam_policies == null &&
      module.minimal.precomputed.aws_s3_buckets.minimal.arn == data.aws_s3_bucket.minimal.arn &&
      module.minimal.precomputed.aws_s3_buckets.minimal.bucket_domain_name == data.aws_s3_bucket.minimal.bucket_domain_name &&
      module.minimal.precomputed.aws_s3_buckets.minimal.id == data.aws_s3_bucket.minimal.id &&
      module.minimal.precomputed.kms_aws_kms_alias == null &&
      module.minimal.precomputed.kms_aws_iam_policies == null &&
      module.minimal.precomputed.aws_s3_buckets.minimal.id == data.aws_s3_bucket.minimal.id &&
      module.minimal.precomputed.aws_iam_policies == null &&
      module.minimal.precomputed.aws_s3_access_point == null
    )
    error_message = "Minimal S3 bucket functional test fails: outputs error"
  }

  assert {
    condition = (
      length(jsondecode(data.aws_s3_bucket_policy.minimal.policy).Statement) == 1 &&
      jsondecode(data.aws_s3_bucket_policy.minimal.policy).Statement[0].Action == "s3:*" &&
      jsondecode(data.aws_s3_bucket_policy.minimal.policy).Statement[0].Principal == { AWS : "*" } &&
      contains(jsondecode(data.aws_s3_bucket_policy.minimal.policy).Statement[0].Resource, data.aws_s3_bucket.minimal.arn) &&
      contains(jsondecode(data.aws_s3_bucket_policy.minimal.policy).Statement[0].Resource, "${data.aws_s3_bucket.minimal.arn}/*") &&
      jsondecode(data.aws_s3_bucket_policy.minimal.policy).Statement[0].Effect == "Deny" &&
      jsondecode(data.aws_s3_bucket_policy.minimal.policy).Statement[0].Condition == { Bool : { "aws:SecureTransport" : "false" } }
    )
    error_message = "Minimal S3 bucket functional test fails: resource-based policy"
  }
}

###
# Complete
###

data "aws_s3_bucket" "complete" {
  for_each = module.complete.aws_s3_buckets

  bucket = each.value.id
  depends_on = [
    module.complete
  ]
}

data "aws_s3_bucket_policy" "complete" {
  for_each = module.complete.aws_s3_buckets

  bucket = each.value.id

  depends_on = [
    module.complete
  ]
}

data "aws_kms_key" "complete" {
  key_id = module.complete.aws_kms_key.id

  depends_on = [
    module.complete
  ]
}

data "aws_kms_alias" "complete" {
  name = module.complete.aws_kms_alias.name

  depends_on = [
    module.complete
  ]
}

check "complete" {
  assert {
    condition = (
      module.complete.aws_s3_buckets.complete.arn == data.aws_s3_bucket.complete["complete"].arn &&
      module.complete.aws_s3_buckets.lock.arn == data.aws_s3_bucket.complete["lock"].arn &&
      startswith(module.complete.aws_s3_buckets.complete.id, local.prefix) &&
      length(module.complete.aws_s3_access_point) == 2 &&
      module.complete.aws_s3_access_point.complete_public.arn == module.complete.precomputed.aws_s3_access_point.complete_public.arn &&
      module.complete.aws_s3_access_point.complete_public.name == module.complete.precomputed.aws_s3_access_point.complete_public.name &&
      module.complete.aws_s3_access_point.complete_public.domain_name == module.complete.precomputed.aws_s3_access_point.complete_public.domain_name &&
      startswith(module.complete.aws_s3_access_point.complete_public.name, local.prefix) &&
      module.complete.aws_s3_access_point.complete_vpc.arn == module.complete.precomputed.aws_s3_access_point.complete_vpc.arn &&
      module.complete.aws_s3_access_point.complete_vpc.name == module.complete.precomputed.aws_s3_access_point.complete_vpc.name &&
      module.complete.aws_s3_access_point.complete_vpc.domain_name == module.complete.precomputed.aws_s3_access_point.complete_vpc.domain_name &&
      startswith(module.complete.aws_s3_access_point.complete_vpc.name, local.prefix) &&
      length(module.complete.aws_iam_policies) == 5 &&
      contains(keys(module.complete.aws_iam_policies), "full") &&
      contains(keys(module.complete.aws_iam_policies), "ro") &&
      contains(keys(module.complete.aws_iam_policies), "rw") &&
      contains(keys(module.complete.aws_iam_policies), "rwd") &&
      contains(keys(module.complete.aws_iam_policies), "audit") &&
      module.complete.aws_iam_policies.full.arn == module.complete.precomputed.aws_iam_policies.full.arn &&
      module.complete.aws_iam_policies.ro.arn == module.complete.precomputed.aws_iam_policies.ro.arn &&
      module.complete.aws_iam_policies.rw.arn == module.complete.precomputed.aws_iam_policies.rw.arn &&
      module.complete.aws_iam_policies.rwd.arn == module.complete.precomputed.aws_iam_policies.rwd.arn &&
      module.complete.aws_iam_policies.audit.arn == module.complete.precomputed.aws_iam_policies.audit.arn &&
      module.complete.aws_iam_policies.full.name == module.complete.precomputed.aws_iam_policies.full.name &&
      module.complete.aws_iam_policies.ro.name == module.complete.precomputed.aws_iam_policies.ro.name &&
      module.complete.aws_iam_policies.rw.name == module.complete.precomputed.aws_iam_policies.rw.name &&
      module.complete.aws_iam_policies.rwd.name == module.complete.precomputed.aws_iam_policies.rwd.name &&
      module.complete.aws_iam_policies.audit.name == module.complete.precomputed.aws_iam_policies.audit.name &&
      contains(keys(module.complete.aws_iam_policies.full), "json") &&
      contains(keys(module.complete.aws_iam_policies.ro), "json") &&
      contains(keys(module.complete.aws_iam_policies.rw), "json") &&
      contains(keys(module.complete.aws_iam_policies.rwd), "json") &&
      contains(keys(module.complete.aws_iam_policies.audit), "json") &&
      module.complete.aws_s3_bucket_website_configurations == null &&
      module.complete.aws_kms_key.arn == data.aws_kms_key.complete.arn &&
      module.complete.aws_kms_key.id == data.aws_kms_key.complete.id &&
      module.complete.precomputed.kms_aws_kms_alias.arns[0] == data.aws_kms_alias.complete.arn &&
      module.complete.aws_kms_alias.arn == data.aws_kms_alias.complete.arn &&
      startswith(module.complete.aws_kms_alias.name, "alias/${local.prefix}") &&
      module.complete.aws_kms_alias.name == data.aws_kms_alias.complete.name &&
      module.complete.aws_kms_grants == null &&
      module.complete.kms_aws_iam_policies == null &&
      module.complete.precomputed.aws_s3_buckets.complete.arn == data.aws_s3_bucket.complete["complete"].arn &&
      module.complete.precomputed.aws_s3_buckets.complete.bucket_domain_name == data.aws_s3_bucket.complete["complete"].bucket_domain_name &&
      module.complete.precomputed.aws_s3_buckets.complete.id == data.aws_s3_bucket.complete["complete"].id &&
      module.complete.precomputed.aws_s3_buckets.lock.arn == data.aws_s3_bucket.complete["lock"].arn &&
      module.complete.precomputed.aws_s3_buckets.lock.bucket_domain_name == data.aws_s3_bucket.complete["lock"].bucket_domain_name &&
      module.complete.precomputed.aws_s3_buckets.lock.id == data.aws_s3_bucket.complete["lock"].id &&
      contains(keys(module.complete.precomputed.aws_iam_policies), "full") &&
      contains(keys(module.complete.precomputed.aws_iam_policies), "ro") &&
      contains(keys(module.complete.precomputed.aws_iam_policies), "rw") &&
      contains(keys(module.complete.precomputed.aws_iam_policies), "rwd") &&
      contains(keys(module.complete.precomputed.aws_iam_policies), "audit")
    )
    error_message = "Complete S3 bucket functional test fails: outputs error"
  }

  assert {
    condition = (
      length(jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement) == 9 &&
      jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[0].Action == "s3:*" &&
      jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[0].Principal == { AWS : "*" } &&
      contains(jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[0].Resource, data.aws_s3_bucket.complete["complete"].arn) &&
      contains(jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[0].Resource, "${data.aws_s3_bucket.complete["complete"].arn}/*") &&
      jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[0].Effect == "Deny" &&
      jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[0].Condition == { Bool : { "aws:SecureTransport" : "false" } } &&

      jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[1].Action == "s3:*" &&
      jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[1].Principal == { AWS : "*" } &&
      contains(jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[1].Resource, data.aws_s3_bucket.complete["complete"].arn) &&
      contains(jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[1].Resource, "${data.aws_s3_bucket.complete["complete"].arn}/*") &&
      jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[1].Effect == "Allow" &&
      length(jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[1].Condition.StringLike["aws:sourceArn"]) == 3 &&
      contains(jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[1].Condition.StringLike["aws:sourceArn"], "arn:aws:foo:*:${data.aws_caller_identity.current.account_id}:foo/SourceArn") &&
      contains(jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[1].Condition.StringLike["aws:sourceArn"], "arn:aws:foo:us-east-1:${data.aws_caller_identity.current.account_id}:foo/SourceArn2") &&
      contains(jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[1].Condition.StringLike["aws:sourceArn"], "arn:aws:foo::${data.aws_caller_identity.current.account_id}:foo/SourceArn3") &&

      jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[2].Action == "s3:*" &&
      jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[2].Principal == { AWS : "*" } &&
      contains(jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[2].Resource, data.aws_s3_bucket.complete["complete"].arn) &&
      contains(jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[2].Resource, "${data.aws_s3_bucket.complete["complete"].arn}/*") &&
      jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[2].Effect == "Deny" &&
      length(jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[2].Condition.StringNotLike["aws:principalArn"]) == 7 &&
      contains(jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[2].Condition.StringNotLike["aws:principalArn"], "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/AdminRole") &&
      contains(jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[2].Condition.StringNotLike["aws:principalArn"], "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/AuditRole") &&
      contains(jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[2].Condition.StringNotLike["aws:principalArn"], "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/FullRole") &&
      contains(jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[2].Condition.StringNotLike["aws:principalArn"], "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/RwRole") &&
      contains(jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[2].Condition.StringNotLike["aws:principalArn"], "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/RwRole2") &&
      contains(jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[2].Condition.StringNotLike["aws:principalArn"], "arn:aws:iam::${data.aws_caller_identity.current.account_id}:user/RoRole") &&
      contains(jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[2].Condition.StringNotLike["aws:principalArn"], "arn:aws:sts:*:${data.aws_caller_identity.current.account_id}:role/RwdRole") &&
      length(jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[2].Condition.StringNotLike["aws:sourceArn"]) == 3 &&
      contains(jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[2].Condition.StringNotLike["aws:sourceArn"], "arn:aws:foo:*:${data.aws_caller_identity.current.account_id}:foo/SourceArn") &&
      contains(jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[2].Condition.StringNotLike["aws:sourceArn"], "arn:aws:foo:us-east-1:${data.aws_caller_identity.current.account_id}:foo/SourceArn2") &&
      contains(jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[2].Condition.StringNotLike["aws:sourceArn"], "arn:aws:foo::${data.aws_caller_identity.current.account_id}:foo/SourceArn3") &&

      length(jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[3].NotAction) == 20 &&
      jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[3].Principal == { AWS : "*" } &&
      contains(jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[3].Resource, data.aws_s3_bucket.complete["complete"].arn) &&
      contains(jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[3].Resource, "${data.aws_s3_bucket.complete["complete"].arn}/*") &&
      jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[3].Effect == "Deny" &&
      jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[3].Condition.StringLike["aws:principalArn"] == "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/AuditRole" &&

      length(jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[4].NotAction) == 2 &&
      contains(jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[4].NotAction, "s3:ListBucket") &&
      contains(jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[4].NotAction, "s3:GetObject*") &&
      jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[4].Principal == { AWS : "*" } &&
      contains(jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[4].Resource, data.aws_s3_bucket.complete["complete"].arn) &&
      contains(jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[4].Resource, "${data.aws_s3_bucket.complete["complete"].arn}/*") &&
      jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[4].Effect == "Deny" &&
      jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[4].Condition.StringLike["aws:principalArn"] == "arn:aws:iam::${data.aws_caller_identity.current.account_id}:user/RoRole" &&
      length(jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[4].Condition.StringLike["aws:sourceArn"]) == 3 &&
      contains(jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[4].Condition.StringLike["aws:sourceArn"], "arn:aws:foo:*:${data.aws_caller_identity.current.account_id}:foo/SourceArn") &&
      contains(jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[4].Condition.StringLike["aws:sourceArn"], "arn:aws:foo:us-east-1:${data.aws_caller_identity.current.account_id}:foo/SourceArn2") &&
      contains(jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[4].Condition.StringLike["aws:sourceArn"], "arn:aws:foo::${data.aws_caller_identity.current.account_id}:foo/SourceArn3") &&

      length(jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[5].NotAction) == 5 &&
      contains(jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[5].NotAction, "s3:ListBucket") &&
      contains(jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[5].NotAction, "s3:GetObject*") &&
      contains(jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[5].NotAction, "s3:RestoreObject") &&
      contains(jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[5].NotAction, "s3:ReplicateObject") &&
      contains(jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[5].NotAction, "s3:PutObject*") &&
      jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[5].Principal == { AWS : "*" } &&
      contains(jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[5].Resource, data.aws_s3_bucket.complete["complete"].arn) &&
      contains(jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[5].Resource, "${data.aws_s3_bucket.complete["complete"].arn}/*") &&
      jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[5].Effect == "Deny" &&
      length(jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[5].Condition.StringLike["aws:principalArn"]) == 2 &&
      contains(jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[5].Condition.StringLike["aws:principalArn"], "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/RwRole") &&
      contains(jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[5].Condition.StringLike["aws:principalArn"], "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/RwRole2") &&

      length(jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[6].NotAction) == 6 &&
      contains(jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[6].NotAction, "s3:ListBucket") &&
      contains(jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[6].NotAction, "s3:GetObject*") &&
      contains(jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[6].NotAction, "s3:RestoreObject") &&
      contains(jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[6].NotAction, "s3:ReplicateObject") &&
      contains(jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[6].NotAction, "s3:PutObject*") &&
      contains(jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[6].NotAction, "s3:DeleteObject*") &&
      jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[6].Principal == { AWS : "*" } &&
      contains(jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[6].Resource, data.aws_s3_bucket.complete["complete"].arn) &&
      contains(jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[6].Resource, "${data.aws_s3_bucket.complete["complete"].arn}/*") &&
      jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[6].Effect == "Deny" &&
      jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[6].Condition.StringLike["aws:principalArn"] == "arn:aws:sts:*:${data.aws_caller_identity.current.account_id}:role/RwdRole" &&

      jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[7].Action == "s3:*" &&
      jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[7].Principal == { AWS : "*" } &&
      contains(jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[7].Resource, data.aws_s3_bucket.complete["complete"].arn) &&
      contains(jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[7].Resource, "${data.aws_s3_bucket.complete["complete"].arn}/*") &&
      jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[7].Effect == "Deny" &&
      jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[7].Condition.StringNotEquals["aws:PrincipalAccount"] == data.aws_caller_identity.current.account_id &&

      jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[8].Action == "s3:*" &&
      jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[8].Principal == { AWS : "*" } &&
      contains(jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[8].Resource, data.aws_s3_bucket.complete["complete"].arn) &&
      contains(jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[8].Resource, "${data.aws_s3_bucket.complete["complete"].arn}/*") &&
      jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[8].Effect == "Deny" &&
      jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[8].Condition.StringNotEquals["aws:SourceAccount"] == data.aws_caller_identity.current.account_id &&
      jsondecode(data.aws_s3_bucket_policy.complete["complete"].policy).Statement[8].Condition.BoolIfExists["aws:ViaAWSService"] == "true"
    )
    error_message = "Complete S3 bucket functional test fails: resource-based policy"
  }

  assert {
    condition = (
      length(jsondecode(module.complete.aws_iam_policies.ro.json).Statement) == 2 &&
      jsondecode(module.complete.aws_iam_policies.ro.json).Statement[0].Action == "kms:Decrypt" &&
      jsondecode(module.complete.aws_iam_policies.ro.json).Statement[0].Effect == "Allow" &&
      jsondecode(module.complete.aws_iam_policies.ro.json).Statement[0].Resource == data.aws_kms_alias.complete.arn &&
      length(jsondecode(module.complete.aws_iam_policies.ro.json).Statement[1].Action) == 2 &&
      contains(jsondecode(module.complete.aws_iam_policies.ro.json).Statement[1].Action, "s3:ListBucket") &&
      contains(jsondecode(module.complete.aws_iam_policies.ro.json).Statement[1].Action, "s3:GetObject*") &&
      jsondecode(module.complete.aws_iam_policies.ro.json).Statement[1].Effect == "Allow" &&
      length(jsondecode(module.complete.aws_iam_policies.ro.json).Statement[1].Resource) == 4 &&
      contains(jsondecode(module.complete.aws_iam_policies.ro.json).Statement[1].Resource, data.aws_s3_bucket.complete["complete"].arn) &&
      contains(jsondecode(module.complete.aws_iam_policies.ro.json).Statement[1].Resource, "${data.aws_s3_bucket.complete["complete"].arn}/*") &&
      contains(jsondecode(module.complete.aws_iam_policies.ro.json).Statement[1].Resource, data.aws_s3_bucket.complete["lock"].arn) &&
      contains(jsondecode(module.complete.aws_iam_policies.ro.json).Statement[1].Resource, "${data.aws_s3_bucket.complete["lock"].arn}/*") &&

      length(jsondecode(module.complete.aws_iam_policies.rw.json).Statement) == 2 &&
      length(jsondecode(module.complete.aws_iam_policies.rw.json).Statement[0].Action) == 2 &&
      contains(jsondecode(module.complete.aws_iam_policies.rw.json).Statement[0].Action, "kms:Decrypt") &&
      contains(jsondecode(module.complete.aws_iam_policies.rw.json).Statement[0].Action, "kms:GenerateDataKey") &&
      jsondecode(module.complete.aws_iam_policies.rw.json).Statement[0].Effect == "Allow" &&
      jsondecode(module.complete.aws_iam_policies.rw.json).Statement[0].Resource == data.aws_kms_alias.complete.arn &&
      length(jsondecode(module.complete.aws_iam_policies.rw.json).Statement[1].Action) == 5 &&
      contains(jsondecode(module.complete.aws_iam_policies.rw.json).Statement[1].Action, "s3:RestoreObject") &&
      contains(jsondecode(module.complete.aws_iam_policies.rw.json).Statement[1].Action, "s3:ReplicateObject") &&
      contains(jsondecode(module.complete.aws_iam_policies.rw.json).Statement[1].Action, "s3:PutObject*") &&
      contains(jsondecode(module.complete.aws_iam_policies.rw.json).Statement[1].Action, "s3:ListBucket") &&
      contains(jsondecode(module.complete.aws_iam_policies.rw.json).Statement[1].Action, "s3:GetObject*") &&
      jsondecode(module.complete.aws_iam_policies.rw.json).Statement[1].Effect == "Allow" &&
      length(jsondecode(module.complete.aws_iam_policies.rw.json).Statement[1].Resource) == 4 &&
      contains(jsondecode(module.complete.aws_iam_policies.rw.json).Statement[1].Resource, data.aws_s3_bucket.complete["complete"].arn) &&
      contains(jsondecode(module.complete.aws_iam_policies.rw.json).Statement[1].Resource, "${data.aws_s3_bucket.complete["complete"].arn}/*") &&
      contains(jsondecode(module.complete.aws_iam_policies.rw.json).Statement[1].Resource, data.aws_s3_bucket.complete["lock"].arn) &&
      contains(jsondecode(module.complete.aws_iam_policies.rw.json).Statement[1].Resource, "${data.aws_s3_bucket.complete["lock"].arn}/*") &&

      length(jsondecode(module.complete.aws_iam_policies.rwd.json).Statement) == 2 &&
      length(jsondecode(module.complete.aws_iam_policies.rwd.json).Statement[0].Action) == 2 &&
      contains(jsondecode(module.complete.aws_iam_policies.rwd.json).Statement[0].Action, "kms:Decrypt") &&
      contains(jsondecode(module.complete.aws_iam_policies.rwd.json).Statement[0].Action, "kms:GenerateDataKey") &&
      jsondecode(module.complete.aws_iam_policies.rwd.json).Statement[0].Effect == "Allow" &&
      jsondecode(module.complete.aws_iam_policies.rwd.json).Statement[0].Resource == data.aws_kms_alias.complete.arn &&
      length(jsondecode(module.complete.aws_iam_policies.rwd.json).Statement[1].Action) == 6 &&
      contains(jsondecode(module.complete.aws_iam_policies.rwd.json).Statement[1].Action, "s3:DeleteObject*") &&
      contains(jsondecode(module.complete.aws_iam_policies.rwd.json).Statement[1].Action, "s3:RestoreObject") &&
      contains(jsondecode(module.complete.aws_iam_policies.rwd.json).Statement[1].Action, "s3:ReplicateObject") &&
      contains(jsondecode(module.complete.aws_iam_policies.rwd.json).Statement[1].Action, "s3:PutObject*") &&
      contains(jsondecode(module.complete.aws_iam_policies.rwd.json).Statement[1].Action, "s3:ListBucket") &&
      contains(jsondecode(module.complete.aws_iam_policies.rwd.json).Statement[1].Action, "s3:GetObject*") &&
      jsondecode(module.complete.aws_iam_policies.rwd.json).Statement[1].Effect == "Allow" &&
      length(jsondecode(module.complete.aws_iam_policies.rwd.json).Statement[1].Resource) == 4 &&
      contains(jsondecode(module.complete.aws_iam_policies.rwd.json).Statement[1].Resource, data.aws_s3_bucket.complete["complete"].arn) &&
      contains(jsondecode(module.complete.aws_iam_policies.rwd.json).Statement[1].Resource, "${data.aws_s3_bucket.complete["complete"].arn}/*") &&
      contains(jsondecode(module.complete.aws_iam_policies.rwd.json).Statement[1].Resource, data.aws_s3_bucket.complete["lock"].arn) &&
      contains(jsondecode(module.complete.aws_iam_policies.rwd.json).Statement[1].Resource, "${data.aws_s3_bucket.complete["lock"].arn}/*") &&

      length(jsondecode(module.complete.aws_iam_policies.full.json).Statement) == 2 &&
      jsondecode(module.complete.aws_iam_policies.full.json).Statement[0].Action == "kms:*" &&
      jsondecode(module.complete.aws_iam_policies.full.json).Statement[0].Effect == "Allow" &&
      jsondecode(module.complete.aws_iam_policies.full.json).Statement[0].Resource == data.aws_kms_alias.complete.arn &&
      jsondecode(module.complete.aws_iam_policies.full.json).Statement[1].Action == "s3:*" &&
      jsondecode(module.complete.aws_iam_policies.full.json).Statement[1].Effect == "Allow" &&
      length(jsondecode(module.complete.aws_iam_policies.full.json).Statement[1].Resource) == 4 &&
      contains(jsondecode(module.complete.aws_iam_policies.full.json).Statement[1].Resource, data.aws_s3_bucket.complete["complete"].arn) &&
      contains(jsondecode(module.complete.aws_iam_policies.full.json).Statement[1].Resource, "${data.aws_s3_bucket.complete["complete"].arn}/*") &&
      contains(jsondecode(module.complete.aws_iam_policies.full.json).Statement[1].Resource, data.aws_s3_bucket.complete["lock"].arn) &&
      contains(jsondecode(module.complete.aws_iam_policies.full.json).Statement[1].Resource, "${data.aws_s3_bucket.complete["lock"].arn}/*") &&

      length(jsondecode(module.complete.aws_iam_policies.audit.json).Statement) == 2 &&
      length(jsondecode(module.complete.aws_iam_policies.audit.json).Statement[0].Action) == 5 &&
      contains(jsondecode(module.complete.aws_iam_policies.audit.json).Statement[0].Action, "kms:ListResourceTags") &&
      contains(jsondecode(module.complete.aws_iam_policies.audit.json).Statement[0].Action, "kms:GetPublicKey") &&
      contains(jsondecode(module.complete.aws_iam_policies.audit.json).Statement[0].Action, "kms:GetKeyRotationStatus") &&
      contains(jsondecode(module.complete.aws_iam_policies.audit.json).Statement[0].Action, "kms:GetKeyPolicy") &&
      contains(jsondecode(module.complete.aws_iam_policies.audit.json).Statement[0].Action, "kms:DescribeKey") &&
      jsondecode(module.complete.aws_iam_policies.audit.json).Statement[0].Effect == "Allow" &&
      jsondecode(module.complete.aws_iam_policies.audit.json).Statement[0].Resource == data.aws_kms_alias.complete.arn &&
      length(jsondecode(module.complete.aws_iam_policies.audit.json).Statement[1].Action) == 20 &&
      jsondecode(module.complete.aws_iam_policies.audit.json).Statement[1].Effect == "Allow" &&
      length(jsondecode(module.complete.aws_iam_policies.audit.json).Statement[1].Resource) == 4 &&
      contains(jsondecode(module.complete.aws_iam_policies.audit.json).Statement[1].Resource, data.aws_s3_bucket.complete["complete"].arn) &&
      contains(jsondecode(module.complete.aws_iam_policies.audit.json).Statement[1].Resource, "${data.aws_s3_bucket.complete["complete"].arn}/*") &&
      contains(jsondecode(module.complete.aws_iam_policies.audit.json).Statement[1].Resource, data.aws_s3_bucket.complete["lock"].arn) &&
      contains(jsondecode(module.complete.aws_iam_policies.audit.json).Statement[1].Resource, "${data.aws_s3_bucket.complete["lock"].arn}/*")
    )
    error_message = "Complete S3 bucket functional test fails: identity-based policy"
  }

  assert {
    condition     = data.aws_region.current.name == "us-east-1"
    error_message = "Complete S3 bucket functional test fails: KMS key policy"
  }
}
