#####
# Context
#####

locals {
  prefix     = "${random_string.start_letter.result}${random_string.this.result}"
  caller_arn = replace(replace(join("/", chunklist(split("/", data.aws_caller_identity.current.arn), 2)[0]), "sts", "iam"), "assumed-role", "role")
}

resource "random_string" "start_letter" {
  length  = 1
  upper   = false
  special = false
  numeric = false
}

resource "random_string" "this" {
  length  = 3
  upper   = false
  special = false
}

resource "aws_iam_role" "test" {
  name = "tftestsecrets"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action    = "sts:AssumeRole"
        Effect    = "Allow"
        Sid       = ""
        Principal = { AWS = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:root" }
      },
    ]
  })
}

data "aws_vpc" "default" {
  default = true
}

data "aws_caller_identity" "current" {}

data "aws_region" "current" {}

#####
# Minimal
#####

module "minimal" {
  source = "../../"

  testing_prefix = local.prefix

  buckets = {
    minimal = {
      name          = "tftestminimal"
      force_destroy = true
    }
  }

  providers = {
    aws.replica = aws.replica
  }
}

#####
# Complete
# - Multiple buckets
# - Policies
# - KMS
# - Logging
# - Access Points
# - Versioning
# - Lifecycle
# - Object Lock
# - …
#####

module "complete" {
  source = "../../"

  testing_prefix = local.prefix
  buckets = {
    complete = {
      name                    = "tftestcomplete"
      block_public_acls       = true
      ignore_public_acls      = true
      block_public_policy     = false
      restrict_public_buckets = false
      force_destroy           = true
      tags = {
        override = "bucket-level"
      }
    }

    lock = {
      name                    = "tftestlock"
      block_public_acls       = true
      block_public_policy     = true
      restrict_public_buckets = true
      ignore_public_acls      = true
      force_destroy           = true
      sse_enabled             = false
      bucket_object_ownership = "BucketOwnerEnforced"
    }
  }

  kms_key = {
    alias                   = "S3tftestcomplete"
    description             = "${local.prefix}S3tftestcomplete KMS key example for S3"
    rotation_enabled        = true
    deletion_window_in_days = 7
    tags = {
      purpose = "tftestS3"
    }
  }

  iam_policy_restrict_by_account_ids = [data.aws_caller_identity.current.account_id]
  iam_policy_restrict_by_regions     = [data.aws_region.current.name]
  iam_policy_entity_arns = {
    ro = {
      0 = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:user/RoRole"
    }
    rw = {
      0 = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/RwRole"
      1 = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/RwRole2"
    }
    rwd = {
      wildcard_region = "arn:aws:sts:*:${data.aws_caller_identity.current.account_id}:role/RwdRole"
    }
    audit = {
      some_role = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/AuditRole"
    }
    full = {
      call        = local.caller_arn,
      random_role = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/FullRole"
    }
  }
  iam_policy_source_arns = {
    ro = {
      wildcard_region = "arn:aws:foo:*:${data.aws_caller_identity.current.account_id}:foo/SourceArn"
      specific_region = "arn:aws:foo:us-east-1:${data.aws_caller_identity.current.account_id}:foo/SourceArn2"
      no_region       = "arn:aws:foo::${data.aws_caller_identity.current.account_id}:foo/SourceArn3"
    }
  }
  iam_policy_sid_prefix     = local.prefix
  iam_policy_export_actions = true
  iam_policy_export_json    = true
  iam_policy_identity = {
    name_template        = "tftestpolicies%s"
    description_template = "${local.prefix}tftestpolicies for %s"
  }

  logging_configurations = {
    complete = {
      target_bucket = module.minimal.aws_s3_buckets.minimal.id
      target_prefix = "tftest"
    }
  }

  versioning_configurations = {
    complete = {
      status     = true
      mfa_delete = false
    }

    lock = {
      status     = true
      mfa_delete = false
    }
  }

  access_points = {
    complete = {
      vpc = {
        name                     = "complete"
        vpc_configuration_vpc_id = data.aws_vpc.default.id
        # This would create a cycle: this is expected, since the policy needs the access point ARN
        # You can still use the policy here if the policy does not depend on access point ARN
        # For this specific case, just see the solution below
        # policy = data.aws_iam_policy_document.access_point.json
      }

      public = {
        name                                         = "public"
        public_configuration_block_public_acls       = false
        public_configuration_block_public_policy     = false
        public_configuration_restrict_public_buckets = false
      }
    }
  }

  object_lock_configurations = {
    lock = {
      rule = {
        default_retention = {
          mode = "COMPLIANCE"
          days = 1
        }
      }
    }
  }

  lifecycle_configurations_rules = {
    complete = [
      {
        id     = "basic"
        status = "Enabled"
        expiration = {
          days                         = 1
          expired_object_delete_marker = false
        }
        filter = {
          and = {
            0 = {
              object_size_greater_than = 1000
              object_size_less_than    = 100000
              tag = {
                key   = "test"
                value = "test"
              }
            }
          }
        }
      }
    ]
  }

  tags = {
    reason   = "tests"
    override = "default"
  }

  providers = {
    aws.replica = aws.replica
  }
}

# Created outside the module to circumvent dependency cycle
resource "aws_s3control_access_point_policy" "example_vpc" {
  access_point_arn = module.complete.aws_s3_access_point["complete_vpc"].arn
  policy           = data.aws_iam_policy_document.access_point.json
}

data "aws_iam_policy_document" "access_point" {
  statement {
    sid    = "Test"
    effect = "Allow"

    actions = [
      "s3:*",
    ]
    resources = [module.complete.precomputed.aws_s3_access_point["complete_vpc"].arn]
    principals {
      type        = "AWS"
      identifiers = ["*"]
    }
    condition {
      test     = "ForAnyValue:StringLike"
      variable = "aws:PrincipalArn"
      values = [
        replace(replace(join("/", chunklist(split("/", data.aws_caller_identity.current.arn), 2)[0]), "sts", "iam"), "assumed-role", "role")
      ]
    }
  }
}

#####
# External
#####

resource "aws_kms_key" "external" {
  description             = "tftestS3external"
  deletion_window_in_days = 7
}

resource "aws_s3_bucket" "external" {
  bucket        = "tftestexternal-ext"
  force_destroy = true
}

module "external" {
  source = "../../"

  testing_prefix = local.prefix

  buckets = {
    external = {
      name                = "tftestexternal"
      force_destroy       = true
      policy_json_enabled = true
      policy_json         = data.aws_iam_policy_document.for_bucket_external.json
    }
  }
  internal_policy_attachment_enabled = false

  kms_key_id = aws_kms_key.external.id

  iam_policy_identity = {
    name_template        = "tftestexternal%s"
    description_template = "tftestexternal for %s S3"
  }
  iam_policy_source_policy_documents = [data.aws_iam_policy_document.external.json]
  identity_iam_policy_document = {
    resources_arns = [
      aws_s3_bucket.external.arn,
      "${aws_s3_bucket.external.arn}/*"
    ]
  }
  iam_policy_entity_arns = {
    full = {
      call        = local.caller_arn,
      random_role = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/RandomRole"
    }
  }

  providers = {
    aws.replica = aws.replica
  }
}

data "aws_iam_policy_document" "external" {
  statement {
    sid       = "PutObjectPermission"
    effect    = "Allow"
    actions   = ["s3:PutObject"]
    resources = [aws_s3_bucket.external.arn]
  }
}

data "aws_iam_policy_document" "for_bucket_external" {
  statement {
    sid     = "UserNoAccess"
    effect  = "Deny"
    actions = ["s3:*"]
    resources = [
      // For cycle reasons, the ARN must be guessed
      // S3 does not accept wildcards "*"
      "arn:aws:s3:::${local.prefix}tftestexternal",
      "arn:aws:s3:::${local.prefix}tftestexternal/*"
    ]
    principals {
      type        = "AWS"
      identifiers = ["*"]
    }
    condition {
      test     = "StringNotLike"
      values   = [local.caller_arn]
      variable = "aws:principalArn"
    }
  }
}

resource "aws_s3_bucket_policy" "this" {
  bucket = module.external.aws_s3_buckets["external"].id
  policy = data.aws_iam_policy_document.for_bucket_external.json
}

#####
# Static Website
#####

module "static_website" {
  source = "../../"

  testing_prefix = local.prefix
  buckets = {
    website = {
      name          = "tfteststaticwebsite"
      force_destroy = true
    }
  }

  buckets_require_secure_transport      = false
  lifecycle_configuration_default_rules = []

  website_configurations = {
    website = {
      index_document = {
        suffix = "index.html"
      }
      error_document = {
        key = "error.html"
      }
      routing_rule = {
        condition = {
          key_prefix_equals = "docs/"
        }

        redirect = {
          replace_key_prefix_with = "documents/"
        }
      }
    }
  }

  cors_configurations_rules = {
    website = [
      {
        id              = "tftest1"
        allowed_headers = ["*"]
        allowed_methods = ["PUT", "POST"]
        allowed_origins = ["https://s3-website-test.hashicorp.com"]
        expose_headers  = ["ETag"]
        max_age_seconds = 3000
      },
      {
        allowed_methods = ["GET"]
        allowed_origins = ["*"]
      }
    ]
  }

  providers = {
    aws.replica = aws.replica
  }
}

#####
# Account level settings
# Example only - disabled because cannot be parallelized
#####

#    module "account_level_access" {
#      source = "../../"
#
#      account_public_access_block = {
#        block_public_acls       = true
#        block_public_policy     = true
#        ignore_public_acls      = true
#        restrict_public_buckets = true
#      }
#
#      providers = {
#        aws.replica = aws.replica
#      }
#    }
