#####
# Minimal
#####

output "minimal" {
  value = module.minimal
}

#####
# Complete
#####

output "complete" {
  value = module.complete
}

#####
# External
#####

output "external" {
  value = module.external
}

#####
# Static Website
#####

output "static_website" {
  value = module.static_website
}
