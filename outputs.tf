output "precomputed" {
  value = merge(
    {
      aws_s3_buckets = {
        for k, v in var.buckets : k => {
          arn                = provider::aws::arn_build("aws", "s3", "", "", "${var.testing_prefix}${v.name}")
          id                 = "${var.testing_prefix}${v.name}"
          bucket_domain_name = "${var.testing_prefix}${v.name}.s3.amazonaws.com"
        }
      },
      kms_aws_iam_policies = local.kms_should_create ? module.kms["0"].precomputed.aws_iam_policies : null
      kms_aws_kms_alias    = local.kms_should_create ? module.kms["0"].precomputed.aws_kms_aliases["0"] : null
      aws_iam_policies = var.iam_policy_export_actions || var.iam_policy_identity != null ? { for scope in keys(local.iam_scoped_actions) :
        scope => merge(
          var.iam_policy_identity != null ? lookup(module.iam_policy_identity_based["0"].precomputed.aws_iam_policies, scope, null) : null,
          var.iam_policy_export_actions ? { actions = local.iam_scoped_actions[scope], kms_actions = local.kms_iam_actions[scope] } : null,
        )
      } : null
      aws_s3_access_point = length(local.access_points) > 0 ? {
        for k, v in local.access_points : k => {
          arn         = provider::aws::arn_build("aws", "s3", local.current_region, local.current_account_id, "accesspoint/${var.testing_prefix}${v.name}")
          name        = "${var.testing_prefix}${v.name}"
          domain_name = "${var.testing_prefix}${v.name}-${local.current_account_id}.s3-accesspoint.${local.current_region}.amazonaws.com"
        }
      } : null
    }
  )
}
