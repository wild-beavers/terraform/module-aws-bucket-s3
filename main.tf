#####
# Global variables
####

variable "tags" {
  description = "Tags that will be shared with all the resources of this module"
  type        = map(string)
  default     = {}
}

variable "buckets_tags" {
  description = "Map of tags that will be added on the bucket object."
  type        = map(string)
  default     = {}
}

variable "buckets_require_secure_transport" {
  description = "Whether to enforce TLS-only connections to the S3 bucket."
  type        = bool
  default     = true
  nullable    = false
}

variable "buckets" {
  description = <<-DOCUMENTATION
    List of S3 buckets to create. Keys are free value.
    - name                                (required, string): Name of the bucket to create.
    - acl                                 (optional, string, "private"): The canned ACL to apply: <https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_acl>
    - block_public_acls                   (optional, bool, false): Whether Amazon S3 should block public ACLs for this bucket.
    - block_public_policy                 (optional, bool, false): Whether Amazon S3 should block public bucket policies for this bucket.
    - bucket_key_enabled                  (optional, bool, false): Whether or not to use Amazon S3 Bucket Keys for SSE-KMS.
    - bucket_object_ownership             (optional, string, "BucketOwnerPreferred"): Object ownership. Valid values: `BucketOwnerPreferred`: Objects uploaded to the bucket change ownership to the bucket owner if the objects are uploaded with the `bucket-owner-full-control` canned ACL or `ObjectWriter`: The uploading account will own the object if the object is uploaded with the `bucket-owner-full-control` canned ACL or `BucketOwnerEnforced` Bucket owner automatically owns and has full control over every object in the bucket. ACLs no longer affect permissions to data in the S3 bucket.
    - expected_bucket_owner               (optional, string, null): The account ID of the expected bucket owner.
    - force_destroy                       (optional, boolean, false): When set to `true`, calling a deletion of the bucket will work even if the bucket is *not* empty.
    - ignore_public_acls                  (optional, bool, false): Whether Amazon S3 should ignore public ACLs for this bucket.
    - policy_json_enabled                 (optional, bool, false): Whether `json_policy` below is empty or full. Because Terraform catastrophic handling of null values.
    - policy_json                         (optional, string, null): A valid bucket policy JSON document to be applied to the specific bucket. Careful, this comes in complement of IAM entities passed to this module.
    - request_payment_configuration_payer (optional, string, "BucketOwner"): Specifies who pays for the download and request fees. Can be either “BucketOwner” or “Requester”.
    - restrict_public_buckets             (optional, bool, false): Whether Amazon S3 should restrict public bucket policies for this bucket.
    - sse_enabled                         (optional, bool, true): Whether or not to enable server-side encryption. It will use a KMS key created in this module - or external KMS if provided - otherwise, it will use the S3 default encryption.
    - tags                                (optional, map, {}): Tags to apply to the specific bucket, in addition to `var.tags` and `var.bucket_tags`.
DOCUMENTATION
  type = map(object({
    name                                = string
    acl                                 = optional(string, "private")
    block_public_acls                   = optional(bool, true)
    block_public_policy                 = optional(bool, true)
    bucket_key_enabled                  = optional(bool, false)
    bucket_object_ownership             = optional(string, "BucketOwnerPreferred")
    expected_bucket_owner               = optional(string)
    force_destroy                       = optional(bool, false)
    ignore_public_acls                  = optional(bool, true)
    policy_json_enabled                 = optional(bool, false)
    policy_json                         = optional(string)
    request_payment_configuration_payer = optional(string, "BucketOwner")
    restrict_public_buckets             = optional(bool, true)
    sse_enabled                         = optional(bool, true)
    tags                                = optional(map(string), {})
  }))
  default  = {}
  nullable = false

  validation {
    condition = ((!contains([
      for key, bucket in var.buckets :
      (
        contains(["BucketOwner", "Requester"], bucket.request_payment_configuration_payer) &&
        contains(["BucketOwnerPreferred", "ObjectWriter", "BucketOwnerEnforced"], bucket.bucket_object_ownership) &&
        (bucket.expected_bucket_owner == null || can(regex("^[0-9]{12}$", bucket.expected_bucket_owner)))
      )
    ], false)))
    error_message = "One or more “var.buckets” are invalid. Check the requirements in the variables.tf file."
  }
}


variable "versioning_configurations" {
  description = <<-DOCUMENTATION
   Configurations for the versioning of the buckets. Once you enable versioning on a bucket, it can never return to an un-versioned state. You can, however, suspend versioning on that bucket.
   Keys MUST correspond to `var.buckets` keys.

    - status (required, bool):               The versioning state of the bucket. `true` for `Enabled`, `false` for `Suspended`.
    - mfa_delete (optional, bool):           Specifies whether MFA delete is enabled in the bucket versioning configuration. Valid values: `true` for `Enabled` or `false` for `Disabled`.
    - versioning_mfa (optional, string, ""): The concatenation of the authentication device’s serial number, a space, and the value that is displayed on your authentication device. Required if `mfa_delete` is `Enabled`.
DOCUMENTATION

  type = map(object({
    status         = bool
    mfa_delete     = optional(bool)
    versioning_mfa = optional(string, "")
  }))
  default  = {}
  nullable = false
}

variable "website_configurations" {
  description = <<-DOCUMENTATION
   Website configurations for the buckets. See <https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_website_configuration#index_document> for more details.
   Keys MUST correspond to `var.buckets` keys.

    - index_document           (optional, object): The name of the index document for the website. Conflicts with `redirect_all_requests_to`.
      - suffix (required, string): A suffix that is appended to a request that is for a directory on the website endpoint. For example, if the suffix is `index.html` and you make a request to `samplebucket/images/`, the data that is returned will be for the object with the key name `images/index.html`. The suffix must not be empty and must not include a slash character.
    - error_document           (optional, object): The name of the error document for the website. Conflicts with `redirect_all_requests_to`.
      - key (required, string): The object key name to use when a 4XX class error occurs.
    - redirect_all_requests_to (optional, object): Granting permissions. See <https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_logging#target_grant>.
      - host_name (required, string): Name of the host where requests are redirected.
      - protocol  (optional, string): Protocol to use when redirecting requests. The default is the protocol that is used in the original request. Valid values: `http`, `https`.
    - routing_rule             (optional, object): List of rules that define when a redirect is applied and the redirect behavior. Conflicts with `redirect_all_requests_to`.
      - condition (optional, object): Condition that must be met for the specified redirect to apply.
        - http_error_code_returned_equals (optional, string): The HTTP error code when the redirect is applied. If specified with key_prefix_equals, then both must be true for the redirect to be applied. Required if `key_prefix_equals` is not specified.
        - key_prefix_equals               (optional, string): The object key name prefix when the redirect is applied. If specified with `http_error_code_returned_equals`, then both must be true for the redirect to be applied. Required if `http_error_code_returned_equals` is not specified.
      - redirect  (required, object): Redirect information.
        - host_name               (optional, string): The host name to use in the redirect request.
        - http_redirect_code      (optional, string): The HTTP redirect code to use on the response.
        - protocol                (optional, string): Protocol to use when redirecting requests. The default is the protocol that is used in the original request. Valid values: `http`, `https`.
        - replace_key_prefix_with (optional, string): The object key prefix to use in the redirect request. For example, to redirect requests for all pages with prefix `docs/` (objects in the `docs/` folder) to `documents/`, you can set a condition block with `key_prefix_equals` set to `docs/` and in the redirect set `replace_key_prefix_with` to `/documents`. Conflicts with `replace_key_with`.
        - replace_key_with        (optional, string): The specific object key to use in the redirect request. For example, redirect request to `error.html`. Conflicts with `replace_key_prefix_with`.
DOCUMENTATION
  type = map(object({
    index_document = optional(object({
      suffix = string
    }))
    error_document = optional(object({
      key = string
    }))
    redirect_all_requests_to = optional(object({
      host_name = string
      protocol  = optional(string)
    }))
    routing_rule = optional(object({
      condition = optional(object({
        http_error_code_returned_equals = optional(string)
        key_prefix_equals               = optional(string)
      }))
      redirect = optional(object({
        host_name               = optional(string)
        http_redirect_code      = optional(string)
        protocol                = optional(string)
        replace_key_prefix_with = optional(string)
        replace_key_with        = optional(string)
      }))
    }))
  }))
  default  = {}
  nullable = false
}

variable "cors_configurations_rules" {
  description = <<-DOCUMENTATION
    List of CORS configuration rules for the buckets. See <https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_cors_configuration>
    Keys MUST correspond to `var.buckets` keys.

    - allowed_headers (optional, list(string)): Set of Headers that are specified in the `Access-Control-Request-Headers` header.
    - allowed_methods (optional, list(string)): Set of HTTP methods that you allow the origin to execute. Valid values are `GET`, `PUT`, `HEAD`, `POST`, and `DELETE`.
    - allowed_origins (optional, list(string)): Set of origins you want customers to be able to access the bucket from.
    - expose_headers  (optional, list(string)): Set of headers in the response that you want customers to be able to access from their applications (for example, from a JavaScript XMLHttpRequest object).
    - id              (optional, string): Unique identifier for the rule. The value cannot be longer than 255 characters.
    - max_age_seconds (optional, number): The time in seconds that your browser is to cache the preflight response for the specified resource.
DOCUMENTATION
  type = map(list(object({
    allowed_headers = optional(list(string))
    allowed_methods = optional(list(string))
    allowed_origins = optional(list(string))
    expose_headers  = optional(list(string))
    id              = optional(string)
    max_age_seconds = optional(number)
  })))
  default  = {}
  nullable = false
}

variable "lifecycle_configuration_default_rules" {
  // This odd variable (compared to a boolean toggle) comes from the fact Terraform (1.7+) cannot compute…
  // …correct types and it’s not possible to define types in a local, so it seems a variable is the only way to enforce…
  // …the type of these default values.

  description = "Lifecycle default rules configurations for the buckets, according to AWS best practices. Enabled by default, pass an empty list to disable."
  type = list(object({
    id     = string
    status = optional(string, "Enabled")
    abort_incomplete_multipart_upload = optional(object({
      days_after_initiation = number
    }))
    expiration = optional(object({
      date                         = optional(string)
      days                         = optional(number)
      expired_object_delete_marker = optional(bool)
    }))
    filter = optional(object({
      and = optional(map(object({
        object_size_greater_than = optional(number)
        object_size_less_than    = optional(number)
        prefix                   = optional(string)
        tags                     = optional(map(string))
      })))
    }))
    noncurrent_version_expiration = optional(object({
      newer_noncurrent_versions = optional(number)
      noncurrent_days           = optional(number)
    }))
    noncurrent_version_transition = optional(object({
      newer_noncurrent_versions = optional(number)
      noncurrent_days           = optional(number)
      storage_class             = string
    }))
    transition = optional(object({
      date          = optional(string)
      days          = optional(number)
      storage_class = string
    }))
  }))
  default = [
    {
      id     = "abort_incomplete_multipart_upload"
      status = "Enabled"
      abort_incomplete_multipart_upload = {
        days_after_initiation = 7
      }
    }
  ]
  nullable = false
}

variable "lifecycle_configurations_rules" {
  description = <<-DOCUMENTATION
  Lifecycle rules configurations for the buckets. See <https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_lifecycle_configuration> for more details.
  Keys MUST correspond to `var.buckets` keys.

    - id                                (required, string): Unique identifier for the rule. The value cannot be longer than 255 characters.
    - status                            (optional, string, "Enabled"): Whether the rule is currently being applied. Valid values: `Enabled` or `Disabled`.
    - abort_incomplete_multipart_upload (optional, object): Configuration block that specifies the days since the initiation of an incomplete multipart upload that Amazon S3 will wait before permanently removing all parts of the upload.
       - days_after_initiation (required, number): The number of days after which Amazon S3 aborts an incomplete multipart upload.
    - expiration (optional, object): Configuration block that specifies the expiration for the lifecycle of the object in the form of date, days and, whether the object has a delete marker
       - date                         (optional, string): The date the object is to be moved or deleted. Should be in GMT ISO 8601 Format. e.g. `2023-01-13T00:00:00Z`.
       - days                         (optional, number): The lifetime, in days, of the objects that are subject to the rule. The value must be a non-zero positive integer.
       - expired_object_delete_marker (optional, bool): Indicates whether Amazon S3 will remove a delete marker with no noncurrent versions. If set to `true`, the delete marker will be expired; if set to `false` the policy takes no action. Conflicts with `date` and `days`.
    - filter (optional, object): Configuration block used to identify objects that a Lifecycle Rule applies to.
        - and (optional, list(object)): Configuration block used to apply a logical `AND` to two or more predicates. The Lifecycle Rule will apply to any object matching all the predicates configured inside the `and` block.
        - object_size_greater_than (optional, number): Minimum object size to which the rule applies. Value must be at least `0` if specified.
        - object_size_less_than    (optional, number): Maximum object size to which the rule applies. Value must be at least `1` if specified.
        - prefix                   (optional, string): Prefix identifying one or more objects to which the rule applies.
        - tag                      (optional, map(string)): Key-value map of resource tags. All of these tags must exist in the object’s tag set in order for the rule to apply.
    - noncurrent_version_expiration (optional, object): Configuration block that specifies when noncurrent object versions expire.
       - newer_noncurrent_versions (optional, number): The number of noncurrent versions Amazon S3 will retain.
       - noncurrent_days           (optional, number): The number of days an object is noncurrent before Amazon S3 can perform the associated action. Must be a positive integer.
    - noncurrent_version_transition (optional, object): Set of configuration blocks that specify the transition rule for the lifecycle rule that describes when noncurrent objects transition to a specific storage class
       - newer_noncurrent_versions (optional, number): The number of noncurrent versions Amazon S3 will retain.
       - noncurrent_days           (optional, number): The number of days an object is noncurrent before Amazon S3 can perform the associated action. Must be a positive integer.
       - storage_class             (required, string): The class of storage used to store the object. Valid Values: `GLACIER`, `STANDARD_IA`, `ONEZONE_IA`, `INTELLIGENT_TIERING`, `DEEP_ARCHIVE`, `GLACIER_IR`.
    - transition (optional, object): Set of configuration blocks that specify the transition rule for the lifecycle rule that describes when noncurrent objects transition to a specific storage class
      - date          (optional, string): The date objects are transitioned to the specified storage class. The date value must be in ISO 8601 format and set to midnight UTC e.g. `2023-01-13T00:00:00Z`.
      - days          (optional, number): The number of days after creation when objects are transitioned to the specified storage class. The value must be a positive integer. If both `days` and `date` are not specified, defaults to `0`. Valid values depend on `storage_class`.
      - storage_class (required, string): The class of storage used to store the object. Valid Values: `GLACIER`, `STANDARD_IA`, `ONEZONE_IA`, `INTELLIGENT_TIERING`, `DEEP_ARCHIVE`, `GLACIER_IR`.
DOCUMENTATION

  type = map(list(object({
    id     = string
    status = optional(string, "Enabled")
    abort_incomplete_multipart_upload = optional(object({
      days_after_initiation = number
    }))
    expiration = optional(object({
      date                         = optional(string)
      days                         = optional(number)
      expired_object_delete_marker = optional(bool)
    }))
    filter = optional(object({
      and = optional(map(object({
        object_size_greater_than = optional(number)
        object_size_less_than    = optional(number)
        prefix                   = optional(string)
        tags                     = optional(map(string))
      })))
    }))
    noncurrent_version_expiration = optional(object({
      newer_noncurrent_versions = optional(number)
      noncurrent_days           = optional(number)
    }))
    noncurrent_version_transition = optional(object({
      newer_noncurrent_versions = optional(number)
      noncurrent_days           = optional(number)
      storage_class             = string
    }))
    transition = optional(object({
      date          = optional(string)
      days          = optional(number)
      storage_class = string
    }))
  })))
  default  = {}
  nullable = false
}

variable "logging_configurations" {
  description = <<-DOCUMENTATION
   Configure logging on buckets objects. It is highly recommended to use logging for good security practice.
   Keys MUST correspond to `var.buckets` keys.

    - target_bucket (required, string): The bucket where you want Amazon S3 to store server access logs.
    - target_prefix (required, string): A prefix for all log object keys.
    - target_grant  (optional, object): Granting permissions. See <https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_logging#target_grant>.
DOCUMENTATION
  type = map(object({
    target_bucket = string
    target_prefix = string
    target_grant = optional(object({
      grantee = object({
        email_address = optional(string)
        id            = optional(string)
        type          = string
        uri           = optional(string)
      })
      permission = string
    }))
  }))
  default  = {}
  nullable = false
}

variable "object_lock_configurations" {
  description = <<-DOCUMENTATION
    Configure an object lock configuration on the buckets objects. See <https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_object_lock_configuration>.
    Keys MUST correspond to `var.buckets` keys.

    - rule (required, object): The bucket where you want Amazon S3 to store server access logs.
      - default_retention (required, object): The bucket where you want Amazon S3 to store server access logs.
        - mode  (required, string): The default Object Lock retention mode you want to apply to new objects placed in the specified bucket. Valid values: `COMPLIANCE`, `GOVERNANCE`.
        - days  (optional, number): The number of days that you want to specify for the default retention period. Required if `years` is not specified.
        - years (optional, number): The number of years that you want to specify for the default retention period. Required if `days` is not specified.
DOCUMENTATION
  type = map(object({
    rule = object({
      default_retention = object({
        mode  = string
        days  = optional(number)
        years = optional(number)
      })
    })
  }))

  default  = {}
  nullable = false
}

variable "internal_policy_attachment_enabled" {
  description = "Whether to attach internal policies to the buckets. May be disabled for cycle reasons. If disabled, the policy JSON will be outputted in the `bucket_internal_policies` output."
  type        = bool
  default     = true
}

variable "account_public_access_block" {
  description = <<-DOCUMENTATION
    Manages S3 account-level Public Access Block configuration.
    <https://docs.aws.amazon.com/AmazonS3/latest/dev/access-control-block-public-access.html>
    Each AWS account may only have one S3 Public Access Block configuration. Multiple configurations of the resource against the same AWS account will cause a perpetual difference.

    - block_public_acls       (optional, bool, false): Whether Amazon S3 should block public ACLs for buckets in this account.
    - block_public_policy     (optional, bool, false): Whether Amazon S3 should block public bucket policies for buckets in this account.
    - ignore_public_acls      (optional, bool, false): Whether Amazon S3 should ignore public ACLs for buckets in this account.
    - restrict_public_buckets (optional, bool, false): Whether Amazon S3 should restrict public bucket policies for buckets in this account.
DOCUMENTATION
  type = object({
    block_public_acls       = optional(bool, false)
    block_public_policy     = optional(bool, false)
    ignore_public_acls      = optional(bool, false)
    restrict_public_buckets = optional(bool, false)
  })
  default = null
}

#####
# Locals
#####

locals {
  tags = merge(
    var.tags,
    {
      managed-by = "Terraform"
      origin     = "gitlab.com/wild-beavers/terraform/module-aws-bucket-s3"
    },
  )

  lifecycle_configurations_rules_recomputed = { for key, bucket in var.buckets :
    key => try(var.lifecycle_configurations_rules[key], [])
  }
  lifecycle_configurations_rules = { for key, lifecycle_rules in local.lifecycle_configurations_rules_recomputed :
    key => concat(var.lifecycle_configuration_default_rules, lifecycle_rules)
  }

  buckets_with_encryption = { for key, bucket in var.buckets :
    key => merge(bucket, {
      sse_config = {
        sse_algorithm     = local.kms_in_use ? "aws:kms" : "AES256"
        kms_master_key_id = local.kms_key_id
      }
    }) if bucket.sse_enabled
  }

  buckets_with_object_lock = { for key, bucket in var.buckets :
    key => merge(bucket, { object_lock = lookup(var.object_lock_configurations, key, null) }) if contains(keys(var.object_lock_configurations), key)
  }

  buckets_with_versioning = { for key, bucket in var.buckets :
    key => merge(bucket, { versioning = lookup(var.versioning_configurations, key, null) }) if contains(keys(var.versioning_configurations), key)
  }

  buckets_with_logging = { for key, bucket in var.buckets :
    key => merge(bucket, { logging = lookup(var.logging_configurations, key, null) }) if contains(keys(var.logging_configurations), key)
  }

  buckets_with_website = { for key, bucket in var.buckets :
    key => merge(bucket, { website = lookup(var.website_configurations, key, null) }) if contains(keys(var.website_configurations), key)
  }

  buckets_with_lifecycle_rules = { for key, bucket in var.buckets :
    key => merge(bucket, { lifecycle_rules = local.lifecycle_configurations_rules[key] }) if length(local.lifecycle_configurations_rules[key]) > 0
  }

  buckets_with_cors_rules = { for key, bucket in var.buckets :
    key => merge(bucket, { cors_rules = lookup(var.cors_configurations_rules, key, null) }) if contains(keys(var.cors_configurations_rules), key)
  }

  buckets_with_acl = { for key, bucket in var.buckets :
    key => bucket if bucket.acl != "private"
  }

  buckets_with_policy = {
    for key, bucket in var.buckets :
    key => bucket if bucket.policy_json_enabled || length(var.iam_policy_entity_arns) > 0 || var.buckets_require_secure_transport
  }
}

#####
# Account S3 Configuration
#####

resource "aws_s3_account_public_access_block" "this" {
  for_each = var.account_public_access_block != null ? { 0 = 0 } : {}

  block_public_acls       = var.account_public_access_block.block_public_acls
  block_public_policy     = var.account_public_access_block.block_public_policy
  ignore_public_acls      = var.account_public_access_block.ignore_public_acls
  restrict_public_buckets = var.account_public_access_block.restrict_public_buckets
}

#####
# S3 bucket
#####

resource "aws_s3_bucket" "this" {
  for_each = var.buckets

  bucket              = "${var.testing_prefix}${each.value.name}"
  force_destroy       = each.value.force_destroy
  object_lock_enabled = length(var.object_lock_configurations) > 0

  tags = merge(
    {
      Name = "${var.testing_prefix}${each.value.name}"
    },
    local.tags,
    var.buckets_tags,
    each.value.tags,
  )
}

resource "aws_s3_bucket_ownership_controls" "this" {
  for_each = var.buckets

  bucket = aws_s3_bucket.this[each.key].id

  rule {
    object_ownership = each.value.bucket_object_ownership
  }
}

resource "aws_s3_bucket_object_lock_configuration" "this" {
  for_each = local.buckets_with_object_lock

  bucket                = aws_s3_bucket.this[each.key].id
  expected_bucket_owner = each.value.expected_bucket_owner

  rule {
    default_retention {
      mode  = each.value.object_lock.rule.default_retention.mode
      days  = lookup(each.value.object_lock.rule.default_retention, "days", null)
      years = lookup(each.value.object_lock.rule.default_retention, "years", null)
    }
  }
}

resource "aws_s3_bucket_server_side_encryption_configuration" "this" {
  for_each = local.buckets_with_encryption

  bucket                = aws_s3_bucket.this[each.key].bucket
  expected_bucket_owner = each.value.expected_bucket_owner

  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm     = lookup(each.value.sse_config, "sse_algorithm", null)
      kms_master_key_id = lookup(each.value.sse_config, "kms_master_key_id", null)
    }
    bucket_key_enabled = each.value.bucket_key_enabled
  }

  lifecycle {
    precondition {
      condition     = !each.value.bucket_key_enabled || (each.value.bucket_key_enabled && local.kms_in_use)
      error_message = "“bucket_key_enabled” is true for at least one bucket, but no KMS key will be used. When using default encryption, set “bucket_key_enabled” to false"
    }
  }
}

resource "aws_s3_bucket_public_access_block" "this" {
  for_each = var.buckets

  bucket = aws_s3_bucket.this[each.key].id

  block_public_acls       = each.value.block_public_acls
  block_public_policy     = each.value.block_public_policy
  ignore_public_acls      = each.value.ignore_public_acls
  restrict_public_buckets = each.value.restrict_public_buckets

  depends_on = [aws_s3_bucket_policy.this]
}

resource "aws_s3_bucket_request_payment_configuration" "this" {
  for_each = var.buckets

  bucket                = aws_s3_bucket.this[each.key].id
  expected_bucket_owner = each.value.expected_bucket_owner

  payer = each.value.request_payment_configuration_payer
}

resource "aws_s3_bucket_acl" "this" {
  for_each = local.buckets_with_acl

  bucket                = aws_s3_bucket.this[each.key].id
  expected_bucket_owner = each.value.expected_bucket_owner

  acl = each.value.acl
}

resource "aws_s3_bucket_versioning" "this" {
  for_each = local.buckets_with_versioning

  bucket                = aws_s3_bucket.this[each.key].id
  expected_bucket_owner = each.value.expected_bucket_owner

  mfa = each.value.versioning.versioning_mfa

  versioning_configuration {
    status     = each.value.versioning.status ? "Enabled" : "Suspended"
    mfa_delete = each.value.versioning.mfa_delete != null ? (each.value.versioning.mfa_delete ? "Enabled" : "Disabled") : null
  }
}

resource "aws_s3_bucket_logging" "this" {
  for_each = local.buckets_with_logging

  bucket                = aws_s3_bucket.this[each.key].id
  expected_bucket_owner = each.value.expected_bucket_owner

  target_bucket = each.value.logging.target_bucket
  target_prefix = each.value.logging.target_prefix

  dynamic "target_grant" {
    for_each = each.value.logging.target_grant != null ? { 0 = lookup(each.value.logging, "target_grant", {}) } : {}

    content {
      grantee {
        email_address = lookup(target_grant.value.grantee, "email_address", null)
        id            = lookup(target_grant.value.grantee, "id", null)
        type          = target_grant.value.type
        uri           = lookup(target_grant.value.grantee, "uri", null)
      }

      permission = lookup(each.value.logging.target_grant, "permission", null)
    }
  }
}

resource "aws_s3_bucket_website_configuration" "this" {
  for_each = local.buckets_with_website

  bucket                = aws_s3_bucket.this[each.key].id
  expected_bucket_owner = each.value.expected_bucket_owner

  dynamic "index_document" {
    for_each = lookup(each.value.website, "index_document", null) != null ? { 0 = lookup(each.value.website, "index_document", {}) } : {}

    content {
      suffix = index_document.value.suffix
    }
  }

  dynamic "error_document" {
    for_each = lookup(each.value.website, "error_document", null) != null ? { 0 = lookup(each.value.website, "error_document", {}) } : {}

    content {
      key = error_document.value.key
    }
  }

  dynamic "redirect_all_requests_to" {
    for_each = lookup(each.value.website, "redirect_all_requests_to", null) != null ? { 0 = lookup(each.value.website, "redirect_all_requests_to", {}) } : {}

    content {
      host_name = redirect_all_requests_to.value.host_name
      protocol  = lookup(redirect_all_requests_to.value, "protocol", null)
    }
  }

  dynamic "routing_rule" {
    for_each = lookup(each.value.website, "routing_rule", null) != null ? { 0 = lookup(each.value.website, "routing_rule", {}) } : {}

    content {
      dynamic "condition" {
        for_each = lookup(routing_rule.value, "condition", null) != null ? { 0 = lookup(routing_rule.value, "condition", {}) } : {}

        content {
          http_error_code_returned_equals = lookup(condition.value, "http_error_code_returned_equals", null)
          key_prefix_equals               = lookup(condition.value, "key_prefix_equals", null)
        }
      }

      redirect {
        host_name               = lookup(routing_rule.value.redirect, "host_name", null)
        http_redirect_code      = lookup(routing_rule.value.redirect, "http_redirect_code", null)
        protocol                = lookup(routing_rule.value.redirect, "protocol", null)
        replace_key_prefix_with = lookup(routing_rule.value.redirect, "replace_key_prefix_with", null)
        replace_key_with        = lookup(routing_rule.value.redirect, "replace_key_with", null)
      }
    }
  }
}

resource "aws_s3_bucket_lifecycle_configuration" "this" {
  for_each = local.buckets_with_lifecycle_rules

  bucket                = aws_s3_bucket.this[each.key].id
  expected_bucket_owner = each.value.expected_bucket_owner

  dynamic "rule" {
    for_each = each.value.lifecycle_rules

    content {
      id     = rule.value.id
      status = coalesce(rule.value.status, "Enabled")

      dynamic "abort_incomplete_multipart_upload" {
        for_each = lookup(rule.value, "abort_incomplete_multipart_upload", null) != null ? { 0 = lookup(rule.value, "abort_incomplete_multipart_upload", {}) } : {}

        content {
          days_after_initiation = lookup(abort_incomplete_multipart_upload.value, "days_after_initiation", null)
        }
      }

      dynamic "expiration" {
        for_each = lookup(rule.value, "expiration", null) != null ? { 0 = lookup(rule.value, "expiration", {}) } : {}

        content {
          date                         = lookup(expiration.value, "date", null)
          days                         = lookup(expiration.value, "days", null)
          expired_object_delete_marker = lookup(expiration.value, "expired_object_delete_marker", null)
        }
      }

      dynamic "filter" {
        for_each = lookup(rule.value, "filter", null) != null ? { 0 = lookup(rule.value, "filter", {}) } : {}

        content {
          object_size_greater_than = lookup(filter.value, "object_size_greater_than", null)
          object_size_less_than    = lookup(filter.value, "object_size_less_than", null)
          prefix                   = lookup(filter.value, "prefix", null)

          dynamic "and" {
            for_each = lookup(filter.value, "and", null) != null ? lookup(filter.value, "and", {}) : {}

            content {
              object_size_greater_than = lookup(and.value, "object_size_greater_than", null)
              object_size_less_than    = lookup(and.value, "object_size_less_than", null)
              prefix                   = lookup(and.value, "prefix", null)
              tags                     = lookup(and.value, "tags", null)
            }
          }

          dynamic "tag" {
            for_each = lookup(filter.value, "tag", null) != null ? { 0 = lookup(filter.value, "tag", {}) } : {}

            content {
              key   = tag.value.key
              value = tag.value.value
            }
          }
        }
      }

      dynamic "noncurrent_version_expiration" {
        for_each = lookup(rule.value, "noncurrent_version_expiration", null) != null ? { 0 = lookup(rule.value, "noncurrent_version_expiration", {}) } : {}

        content {
          newer_noncurrent_versions = lookup(noncurrent_version_expiration.value, "newer_noncurrent_versions", null)
          noncurrent_days           = lookup(noncurrent_version_expiration.value, "noncurrent_days", null)
        }
      }

      dynamic "noncurrent_version_transition" {
        for_each = lookup(rule.value, "noncurrent_version_transition", null) != null ? { 0 = lookup(rule.value, "noncurrent_version_transition", {}) } : {}

        content {
          newer_noncurrent_versions = lookup(noncurrent_version_transition.value, "newer_noncurrent_versions", null)
          noncurrent_days           = lookup(noncurrent_version_transition.value, "noncurrent_days", null)
          storage_class             = noncurrent_version_transition.value.storage_class
        }
      }

      dynamic "transition" {
        for_each = lookup(rule.value, "transition", null) != null ? { 0 = lookup(rule.value, "transition", {}) } : {}

        content {
          date          = lookup(transition.value, "date", null)
          days          = lookup(transition.value, "days", null)
          storage_class = transition.value.storage_class
        }
      }
    }
  }
}

resource "aws_s3_bucket_cors_configuration" "this" {
  for_each = local.buckets_with_cors_rules

  bucket                = aws_s3_bucket.this[each.key].id
  expected_bucket_owner = each.value.expected_bucket_owner

  dynamic "cors_rule" {
    for_each = each.value.cors_rules

    content {
      allowed_headers = lookup(cors_rule.value, "allowed_headers", null)
      allowed_methods = lookup(cors_rule.value, "allowed_methods", null)
      allowed_origins = lookup(cors_rule.value, "allowed_origins", null)
      expose_headers  = lookup(cors_rule.value, "expose_headers", null)
      max_age_seconds = lookup(cors_rule.value, "max_age_seconds", null)
      id              = lookup(cors_rule.value, "id", null)
    }
  }
}

resource "aws_s3_bucket_policy" "this" {
  for_each = var.internal_policy_attachment_enabled ? local.buckets_with_policy : {}

  bucket = aws_s3_bucket.this[each.key].id

  policy = module.iam_policy_resource_based[each.key].resource_based_aws_iam_policy_document.json
}

####
# Outputs
####

output "aws_s3_buckets" {
  value = {
    for key, bucket in aws_s3_bucket.this : key => {
      for attribute, value in bucket : attribute => value if !(contains(["tags", "website_domain", "website_endpoint", "policy", "lifecycle_rule", "website", "server_side_encryption_configuration", "object_lock_configuration", "logging", "cors_rule", "versioning"], attribute))
    }
  }
}

output "aws_s3_bucket_website_configurations" {
  value = length(local.buckets_with_website) > 0 ? {
    for key, configuration in aws_s3_bucket_website_configuration.this : key => {
      for attribute, value in configuration : attribute => value if(contains(["id", "website_domain", "website_endpoint"], attribute))
    }
  } : null
}
