####
# Variables
####

variable "kms_key_id" {
  type        = string
  default     = null
  description = "ARN or ID (or alias) of the AWS KMS customer master key (CMK) to be used to encrypt the resources data. If you don’t specify this value or `var.kms_key`, then S3 defaults to using the AWS account’s default S3 SSE."
}

variable "kms_key" {
  description = <<-DOCUMENTATION
Options of the KMS key to create.
If given, `var.kms_key_id` will be ignored.

  - alias                   (required, string):      KMS key alias; Display name of the KMS key. Omit the `alias/`.
  - policy_json             (optional, string):      Additional policy to attach to the KMS key, merged with a baseline internal policy. Make sure to provide "var.iam_admin_arns" to avoid getting blocked.
  - description             (optional, string, ""):  Description of the key.
  - rotation_enabled        (optional, bool, true):  Whether to automatically rotate the KMS key linked to the S3.
  - deletion_window_in_days (optional, number, 7):   The waiting period, specified in number of days. After the waiting period ends, AWS KMS deletes the KMS key. If you specify a value, it must be between `7` and `30`, inclusive. If you do not specify a value, it defaults to `7`.
  - tags                    (optional, map(string)): Tags to be used by the KMS key of this module.
DOCUMENTATION
  type = object({
    alias                   = string
    policy_json             = optional(string, null)
    description             = optional(string, "")
    rotation_enabled        = optional(bool, true)
    deletion_window_in_days = optional(number, 7)
    tags                    = optional(map(string), {})
  })
  default = null
}

variable "kms_grants" {
  description = <<-DOCUMENTATION
Grants to create.
Keys are free values.

  - name                  (required, string):       A friendly name for identifying the grant.
  - grantee_principal     (required, string):       The principal that is given permission to perform the operations that the grant permits in ARN format. Note that due to eventual consistency issues around IAM principals, terraform’s state may not always be refreshed to reflect what is true in AWS.
  - operations            (required, list(string)): A list of operations that the grant permits. The permitted values are: `Decrypt`, `Encrypt`, `GenerateDataKey`, `GenerateDataKeyWithoutPlaintext`, `ReEncryptFrom`, `ReEncryptTo`, `Sign`, `Verify`, `GetPublicKey`, `CreateGrant`, `RetireGrant`, `DescribeKey`, `GenerateDataKeyPair`, or `GenerateDataKeyPairWithoutPlaintext`.
  - retiring_principal    (optional, string):       The principal that is given permission to retire the grant by using RetireGrant operation in ARN format. Note that due to eventual consistency issues around IAM principals, terraform’s state may not always be refreshed to reflect what is true in AWS.
  - grant_creation_tokens (optional, list(string)): A list of grant tokens to be used when creating the grant. See Grant Tokens for more information about grant tokens.
  - retire_on_delete      (optional, bool):         If set to false (the default) the grants will be revoked upon deletion, and if set to true the grants will try to be retired upon deletion. Note that retiring grants requires special permissions, hence why we default to revoking grants. See RetireGrant for more information.
DOCUMENTATION
  type = map(map(object({
    name                  = string
    grantee_principal     = string
    operations            = list(string)
    retiring_principal    = optional(string)
    grant_creation_tokens = optional(list(string))
    retire_on_delete      = optional(bool, false)
  })))
  default  = {}
  nullable = false
}

####
# Locals
####

locals {
  kms_should_create = var.kms_key != null

  kms_key_id = local.kms_should_create ? module.kms["0"].aws_kms_aliases["0"].arn : var.kms_key_id
  kms_in_use = local.kms_should_create || var.kms_key_id != null

  kms_iam_ro_actions = ["kms:Decrypt"]
  kms_iam_rw_actions = concat(
    [
      "kms:GenerateDataKey",
    ],
    local.kms_iam_ro_actions
  )
  kms_iam_audit_actions = [
    "kms:DescribeCustomKeyStores",
    "kms:DescribeKey",
    "kms:GetKeyPolicy",
    "kms:GetKeyRotationStatus",
    "kms:GetParametersForImport",
  ]
  kms_iam_actions = {
    // https://docs.aws.amazon.com/AmazonS3/latest/userguide/UsingKMSEncryption.html#aws-managed-customer-managed-cmks
    ro    = local.kms_iam_ro_actions
    rw    = local.kms_iam_rw_actions
    rwd   = local.kms_iam_rw_actions
    audit = local.kms_iam_audit_actions
    full  = ["kms:*"]
  }
}

module "kms" {
  source  = "gitlab.com/wild-beavers/module-aws-kms/aws"
  version = "~> 2"

  for_each = local.kms_should_create ? { 0 = var.kms_key } : {}

  current_region     = var.current_region
  current_account_id = var.current_account_id
  prefix             = var.testing_prefix

  kms_keys = {
    0 = {
      alias             = each.value.alias
      principal_actions = local.kms_iam_actions
      policy_json       = each.value.policy_json

      description             = each.value.description
      rotation_enabled        = each.value.rotation_enabled
      deletion_window_in_days = each.value.deletion_window_in_days
      tags                    = each.value.tags
    }
  }

  kms_grants = var.kms_grants

  replica_enabled = false

  iam_policy_create                  = false
  iam_policy_export_json             = true
  iam_policy_export_actions          = true
  iam_policy_entity_arns             = var.iam_policy_entity_arns
  iam_policy_source_arns             = var.iam_policy_source_arns
  iam_policy_sid_prefix              = var.iam_policy_sid_prefix
  iam_policy_restrict_by_account_ids = var.iam_policy_restrict_by_account_ids
  iam_policy_restrict_by_regions     = var.iam_policy_restrict_by_regions

  tags = local.tags

  providers = {
    aws.replica = aws.replica
  }
}

####
# Outputs
####

output "aws_kms_key" {
  value = local.kms_should_create ? module.kms["0"].aws_kms_keys["0"] : null
}

output "aws_kms_alias" {
  value = local.kms_should_create ? module.kms["0"].aws_kms_aliases["0"] : null
}

output "aws_kms_grants" {
  value = local.kms_should_create && length(var.kms_grants) > 0 ? module.kms["0"].aws_kms_grants["0"] : null
}

output "kms_aws_iam_policies" {
  value = local.kms_should_create ? module.kms["0"].aws_iam_policies : null
}
