# SAFE TO REMOVE AFTER VERSION 9
moved {
  from = aws_iam_policy.this
  to   = module.iam_policy_resource_based["0"].aws_iam_policy.this
}
# END OF SAFE TO REMOVE AFTER VERSION 9
