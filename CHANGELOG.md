## 9.0.3

- maintenance: migrate `wild-beavers` modules to Terraform registry
- chore: bump pre-commit hooks

## 9.0.2

- fix: `aws_s3_bucket.versioning` was causing idempotency issues

## 9.0.1

- fix: some arguments made `precomputed.aws_iam_policies` crash
- fix: `aws_iam_policies` output was tried, even if only actions exports were enabled
- fix: `var.tags` were overriding origin

## 9.0.0

- feat: (BREAKING) removes `var.kms_use_external_key`
- feat: (BREAKING) removes deprecated IAM user creation. This removes `var.iam_users` variable and `iam_users*` outputs
- feat: (BREAKING) change in IAM policy variables:
   - `var.iam_policy_create` => *removed*
   - `var.iam_policy_path` => `var.iam_policy_identity.path`
   - `var.iam_policy_name` => `var.iam_policy_identity.name_template`
   - `var.iam_policy_description` => `var.iam_policy_identity.description_template`
   - `var.iam_policy_external_resources_enabled` => *removed*
   - `var.iam_policy_external_resources_arns` => `var.iam_policy_source_policy_documents`
   - `var.iam_policy_tags` => `var.iam_policy_identity.tags`
- feat: (BREAKING) removes `var.kms_use_external_key` variable
- feat: (BREAKING) moves `var.buckets.xxx.require_secure_transport` to `var.buckets_require_secure_transport`. This setting is now applied to all S3 bucket created by this module
- feat: (BREAKING) change outputs:
   - `buckets` => `aws_s3_buckets`
   - `aws_s3_bucket_website_configurations` => _added_
   - `bucket_internal_policies` => _removed_
   - `access_points` => `aws_s3_access_point`
   - `kms_key.(id|alias|key_id|multi_region)` => `kms_key.(id|alias|key_id|multi_region)`
   - `kms_key.arns` => _removed_
   - `kms_key.alias_(id|arn)` => `aws_kms_alias.(id|arn)`
   - `kms_key.alias_arns` => _removed_
   - `iam_kms_actions` => `precomputed.kms.aws_iam_policy_actions`
   - `aws_kms_alias` => _added_
   - `aws_kms_grants` => _added_
   - `kms_aws_iam_policies` => _added_
   - `precomputed` => _added_
   - `access_points` => `aws_s3_access_point`
   - `iam_policy_jsons.xxx` => `aws_iam_policies.xxx.json`
   - `iam_policy_actions.xxx` => `precomputed.iam_policy_actions.xxx`
   - `iam_policies` => `aws_iam_policies`
   - `kms_key.xxx.(id|alias|key_id|multi_region)` => `aws_kms_keys.xxx.(id|alias|key_id|multi_region)`
   - `kms_key.xxx.arns` => *removed*
   - `kms_key.xxx.alias_(id|arn)` => `aws_kms_aliases.xxx.(id|arn)`
   - `kms_key.xxx.alias_arns` => *removed*
   - `kms_key.xxx.multi_region_replica_yyy` => `replica_aws_kms_keys.xxx.yyy`
   - `kms_key.xxx.multi_region_replica_alias_yyy` => `replica_aws_kms_aliases.xxx.yyy`
   - `kms_key_iam_policy.xxx` => `aws_iam_policies.xxx`
   - `kms_key_iam_policy_jsons` => `precomputed.aws_iam_policy_jsons`
   - `kms_key_iam_actions` => `precomputed.aws_iam_policy_actions`
- feat: adds `var.kms_grants`, `var.current_region`, `var.current_account_id` and `var.testing_prefix` variables
- tech: (BREAKING) bump AWS provider to `5.78` and Terraform version to `1.9`
- maintenance: bump KMS module to `2`

## 8.2.2

- fix: prevent Terraform crash when arn value is `null`

## 8.2.1

- fix: entities allowed in the same account was always granting the highest privileged since policy evaluation don't intersect between entities based policy and resource policy
- chore: bump Terrform minimal version to 1.8
- chore: bump AWS provider minimal version to `5.40`
- chore: bump pre-commit hooks

## 8.2.0

- fix: access denied when the request is not coming from a role listed in `principalArn` and also from `sourceArn`

## 8.1.4

- fix: add allow whitelist statement in internal policy so cross-account resources in `var.iam_policy_entity_arns` are able to access bucket.

## 8.1.3

- fix: external policy creation failed when using scope only on sourceARN

## 8.1.2

- fix: some wildcards could create a blocked internal policy

## 8.1.1

- fix: makes sure internal policy cannot have duplicated values in conditions
- fix: fixes precondition to check for internal policy sanity

## 8.1.0

- feat: adds `var.iam_policy_restrict_by_regions` to allow bucket restriction by region.
- fix: allow wildcard region in `var.iam_policy_source_arns` and `var.iam_policy_entity_arns` validation
- fix: allow service if the account ID match `var.iam_policy_restrict_by_account_ids`

## 8.0.3

- fix: passes `var.iam_policy_source_arns.name` to underlying kms module
- fix: guess S3 ARN in the internal policy to better resist potential cycle
- fix: adds another precondition on internal policy to prevent scopes with empty values

## 8.0.2

- fix: adds `var.access_points.name` to replaces `var.access_points` keys as names. Keys as names still work but is now DEPRECATED.
- fix: adds missing audits permissions
- fix: always set `iam_policy_export_actions` for KMS, as it’s needed for S3 internal policy

## 8.0.1

- fix: guarantees a bucket cannot be blocked because of internal policy

## 8.0.0

- feat: (BREAKING) removes `var.iam_admin_arns` in favor of `var.iam_policy_entity_arns.full`
- feat: (BREAKING) removes `var.iam_policy_group_arns` because it makes the code unmaintainable along with other issues like weak idempotency
- feat: adds default `var.lifecycle_configuration_default_rules` to control new feature
- feat: setup `abort_incomplete_multipart_upload` by default after `7` days
- feat: adds `var.iam_policy_entity_arns.audit` to allow entity to manage S3 without accessing the data
- feat: adds `var.account_public_access_block` to control account-level public access block
- chore: bump pre-commit hooks
- maintenance: pins internal `kms` module to `1` along with required breaking changes
- refactor: removed `moved` from previous major version
- refactor: simplifies code by removing `coalesce` when variables could be `nullable = false` instead
- fix: internal policy was not granting service users access
- fix: service user could break internal policy for other entities

## 7.1.2

- fix: wrong bucket RWD and RO permissions

## 7.1.1

- fix: Add missing lifecycle rule block `abort_incomplete_multipart_upload`.
- fix: examples having invalid value for `days_after_initiation` in `abort_incomplete_multipart_upload`.

## 7.1.0

- feat: adds `require_secure_transport` (default enabled) to for TLS on S3 bucket policy
- feat: output policies jsons if `var.iam_policy_export_json` is set, even if there is no iam entities variables are populated
- chore: bump pre-commit hooks
- test: white-mark resource name

## 7.0.0

- feat: (BREAKING) removes `route53` records for now, since it’s unusable by itself.
- feat: (BREAKING) removes RO/FULL policies created by this module with `var.iam_policy_read_name` and `var.iam_policy_full_name` and removes corresponding outputs
- feat: (BREAKING) adds mandatory `aws.replica` alias to prepare for future possibilities of replication
- feat: (BREAKING) `var.bucket_tags` is now `var.buckets_tags`
- feat: (BREAKING) allow multi buckets creation by adding `var.s3_buckets` - that variable replaces all non-complex type into a map of values:
   - `name`                                (required, string)
   - `acl`                                 (optional, string, "private")
   - `force_destroy`                       (optional, boolean, false)
   - `tags`                                (optional, map, {})
   - `request_payment_configuration_payer` (optional, string, "BucketOwner")
   - `bucket_object_ownership`             (optional, string, "BucketOwnerPreferred")
   - `expected_bucket_owner`               (optional, string, null)
   - `bucket_policy_json`                  (optional, string, null)
   - `sse_enabled`                         (optional, bool, true)
   - `bucket_key_enabled`                  (optional, bool, false)
   - `block_public_acls`                   (optional, bool, true) (BREAKING) now defaults to true
   - `block_public_policy`                 (optional, bool, true) (BREAKING) now defaults to true
   - `ignore_public_acls`                  (optional, bool, true) (BREAKING) now defaults to true
   - `restrict_public_buckets`             (optional, bool, true) (BREAKING) now defaults to true
- feat: (BREAKING) transforms following variables into maps, with keys that must match new `var.buckets` variable:
   - `var.object_lock_configuration` to `var.object_lock_configurations`
   - `var.versioning_configuration` to `var.versioning_configurations`
   - `var.logging_configuration` to `var.logging_configurations`
   - `var.website_configuration` to `var.website_configurations`
   - `var.lifecycle_configuration_rules` to `var.lifecycle_configurations_rules`
   - `var.cors_configuration_rules` to `var.cors_configurations_rules`
- feat: (BREAKING) access points are now scoped under `var.buckets` keys.
- refactor: (BREAKING) removes following outputs in favor of `kms_key` output:
   - `kms_key_arn`
   - `kms_key_id`
   - `kms_alias_arn`
   - `kms_alias_target_key_arn`
- refactor: (BREAKING) removes following outputs in favor of `buckets` output:
   - `id`
   - `arn`
   - `bucket_domain_name`
   - `bucket_regional_domain_name`
   - `hosted_zone_id`
   - `region`
- refactor: (BREAKING) moves `var.versioning_mfa` inside `var.versioning_configurations`
- refactor: (BREAKING) removes all deprecated variables and outputs
- refactor: (BREAKING) outputs `iam_policy_jsons` and `iam_policies` replaces all previous `iam` outputs:
   - `iam_policy_read_only_json`
   - `iam_policy_data_rwd_jsons`
   - `iam_policy_data_rw_jsons`
   - `iam_policy_data_ro_jsons`
- chore: (BREAKING) removes all `moved` from previous major version
- feat: adds `var.iam_policy_source_arns` to allow given source ARN to access S3
- feat: adds `route53` output containing full FQDNs of given `var.route53_records`
- feat: adds `internal_policy_attachment_enabled` to control creation of the internal policy inside the module
- feat: adds variables to automatically manage bucket internal policy
   - `var.iam_admin_arns`
   - `var.iam_policy_restrict_by_account_ids`
   - `var.iam_policy_entity_arns`
   - `var.iam_policy_group_arns`
   - `var.iam_policy_export_actions`
   - `var.iam_policy_export_json`
   - `var.iam_policy_name_template`
   - `var.iam_policy_description`
   - `var.iam_policy_external_resources_enabled`
   - `var.iam_policy_external_resources_arns`
   - `var.buckets.bucket_key_enabled`
   - `var.kms_use_external_key`
- refactor: uses external kms module internally, instead of managing KMS resources
- refactor: re-organize code in better scopes
- chore: updates CHANGELOG to recent practices
- chore: pre-commit deps update

## 6.0.1

- fix: adds missing "kms:GenerateDataKey" for KMS RW policy

## 6.0.0

- maintenance: (BREAKING) bump the minimal Terraform version to 1.3+
- chore: bump pre-commit hooks

## 5.2.0

- feat: adds the ability to add a `iam_policy_sid_prefix`

## 5.1.0

- feat: adds the ability to setup access points with `var.access_points`
- feat: makes `status` for `var.lifecycle_configuration_rules` optional
- feat: deprecates `iam_user_enabled` and related resources. Adds `iam_users` variable and output.
- feat: adds `RWD` data policy with `iam_policy_data_rwd_json` output.
- feat: data policies jsons will be also output by access points
- refactor: data policies will only be created and attached to users.
- refactor: moved `iam_user` and related resources under a `for_each` instead of a count.

## 5.0.1

- maintenance: fix TF lint issues
- chore: bump pre-commit hooks
- test: adds gitlab ci

## 5.0.0

- chore: (BREAKING) pins AWS provider to 4+
- feat: (BREAKING) convert `var.request_payer` to new `aws_s3_bucket_request_payment_configuration` resource
- feat: (BREAKING) remove `var.sse_config` to new `var.sse_enabled` toggle, simplifying the encryption options
- refactor: (BREAKING) change `source_json` to `source_policy_documents`
- refactor: (BREAKING) moves encryption settings to its own resource
- refactor: (BREAKING) change `apply_bucket_policy` to `bucket_policy_enabled`
- refactor: (BREAKING) moves ACL settings to its own resource
- refactor: (BREAKING) moves versioning settings to its own resource
- refactor: (BREAKING) moves logging settings to its own resource
- refactor: (BREAKING) moves website settings to its own resource
- refactor: (BREAKING) moves lifecycle settings to its own resource
- refactor: (BREAKING) moves cors settings to its own resource
- refactor: (BREAKING) moves object_block_configuration settings to its own resource
- refactor: (BREAKING) refactor `versioning_config` in new `versioning_configuration`, with different content
- refactor: (BREAKING) refactor `logging` in new `logging_configuration`, with `grantee` new option content
- refactor: (BREAKING) refactor `static_website` in new `website_configuration`, with different content
- refactor: (BREAKING) refactor `lifecycle_rules` in new `lifecycle_configuration`, with different content
- refactor: (BREAKING) refactor `var.object_lock_configuration` with different content

## 4.0.1

- fix: fixes conditions to create policies for S3 user
- chore: pre-commit dependencies update

## 4.0.0

- feat: adds the ability to create a service user
- feat: adds to add one or more CNAME records linked to the bucket
- feat: nullify outputs that return nothing = null instead of empty string
- feat: outputs JSON for policies, controlled by a new `var.iam_policy_output_jsons`
- feat: (BREAKING) removes `***_descriptions` variables for policies and change them with default values
- feat: adds KMS key JSON for KMS policies
- refactor: (BREAKING) rename `policy_objects_` to `policy_data_` to standardize
- test: changes `var.aws_assume_role` to `var.aws_assume_role_arn`
- chore: removes waiting time for S3 creation by forcing provider 3.73+
- chore: pre-commit dependencies update

## 3.2.0

- feat: add two policy to be used by users who just need to manipulate objects

## 3.1.0

- refactor: removes the `count` on the bucket as the resource will always exists.
- feat: enable type `optional` experimental feature

## 3.0.1

- fix: makes sure KMS key is indeed linked to the bucket when `var.sse_config` is empty

## 3.0.0

- feat: (BREAKING) required at least Terraform 1.0.1+ and AWS provider 3+
- feat: (BREAKING) if `var.kms_key_create` is `true`, key is used
- feat: (BREAKING) if `var.sse_config` now requires `sse_algorithm` and `kms_master_key_id` optionally, not `sse_key` anymore
- feat: (BREAKING) removes `iam_policy_*_policy_document` output as it breaks idempotency and it’s not a critical output
- feat: (BREAKING) removes `var.apply_kms_policy` as it is not used anymore
- feat: (BREAKING) removes `var.enabled` as it is not needed anymore since Terraform 0.13+
- feat: handle tags for policies and add `var.policy_tags` to make it possible to override them
- feat: handles bucket object ownership with new variable `var.bucket_object_ownership`
- fix: makes sure polices also allow KMS alias, not only KMS key
- refactor: use `managed-by`=`Terraform` instead of deprecated `Terraform=true`
- refactor: adds `origin` tag
- refactor: moves Policies data source to `data.tf`
- chore: pre-commit configuration update
- chore: removes pre-commit afcmf as it is handled by gitlab
- chore: aligns .gitignore with modern terraform practices
- test: removes Jenkinsfile
- test: use provider/variables that fits most CI/CD
- test: fixes `MalformedPolicyDocumentException: The new key policy will not allow you to update the key policy in the future` error
- test: removes output descriptions so the module is easier to maintain
- doc: adds LICENSE
- doc: refactors `CHANGELOG.md` to align with most changelogs

## 2.1.0

- feat: add kms_key_rotation_enabled input:
- add support for kms key rotation.
- kms_key_rotation_enabled input variable defaults to true for best security practice.
- test: update examples:
- add standard s3 sse_config in test examples/default/standard-versionned.tf.
- add sse_config with custom kms key in test examples/default/external-kms-no-policies.tf.
- chore: upgrade version and required_providers: upgrade to terraform 0.12.26.

## 2.0.0

- fix: (BREAKING) remove region from bucket definition (needed for aws provider > 3) and `variables.tf`
- fix: permit aws provider > 3
- fix: count on data resources (unbreak when the KMS key is used inside `sse_config`)
- chore: bump pre-commit hooks to fix jenkins test

## 1.0.3

- fix: null value when s3 bucket is not created

## 1.0.2

- fix: wrong IAM polices when default bucket KMS key is used

## 1.0.1

- fix: pre-commit failure due to version bumps

## 1.0.0

- breaking: terraform 0.12 upgrade and new features

## 0.4.0

- Merge branch 'feature/kms_policy' of fxinnovation-public/terraform-module-aws-bucket-s3 into master
- fix/ typo
- feat/ KMS key policy suport

## 0.3.0

- Merge branch 'feature/missing_outputs_add_lifecycle' of fxinnovation-public/terraform-module-aws-bucket-s3 into master
- fix/ typo
- fix/ output + remove prefix
- fix/ typo
- fix/ outputs
- tech/ changelog
- feature/ add object_lock expiration time

## 0.2.0

- Merge branch 'feature/add_object_lock_and_versioning' of fxinnovation-public/terraform-module-aws-bucket-s3 into master
- fix/ typo
- tech/ CHANGELOG
- feature/ add versioning and object_lock

## 0.1.0

- Merge branch 'feature/init' of fxinnovation-public/terraform-module-aws-bucket-s3 into master
- fix/ output format
- tech/ bump pre-commit version
- fix/ stupid terraform 0.11
- fix/ data source
- fix/ bucket policy
- fix/ tests
- feature/ add disable test
- fix/ descriptions
- fix/ typo
- fix/ var name
- fix/ missing comma
- fix/ pre-commit version
- fix/ missing output and missing pre-commit
- feature/ init
- Initial commit
