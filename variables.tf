variable "current_region" {
  description = "The region where this module is run. If not provided, will be fetched dynamically. If given, this value is trusted and will not be checked against the real current region."
  default     = ""
  type        = string
  nullable    = false

  validation {
    condition     = can(regex("^([a-z]{2}-[a-z]{4,10}-[1-9]{1})?$", var.current_region))
    error_message = "“var.current_region” does not match '^([a-z]{2}-[a-z]{4,10}-[1-9]{1})?$'."
  }
}

variable "current_account_id" {
  description = "The account where this module is run. If not provided, will be fetched dynamically. If given, this value is trusted and will not be checked against the real current account ID."
  default     = ""
  type        = string
  nullable    = false

  validation {
    condition     = can(regex("^([0-9]{12})?$", var.current_account_id))
    error_message = "“var.current_account_id” does not match '^[0-9]{12}$'."
  }
}

variable "testing_prefix" {
  description = "Prefix to be used for all resources names. Specifically useful for tests. A 4-characters alphanumeric string. It must start by a lowercase letter."
  type        = string
  default     = ""
  nullable    = false

  validation {
    condition     = var.testing_prefix == "" || can(regex("^[a-z][a-zA-Z0-9]{3}$", var.testing_prefix))
    error_message = "“var.testing_prefix” does not match “^[a-z][a-zA-Z0-9]{3}$”."
  }
}
