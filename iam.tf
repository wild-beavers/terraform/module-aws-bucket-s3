variable "iam_policy_source_policy_documents" {
  description = "JSON to concatenate to create the external policies."
  type        = list(string)
  default     = []
  nullable    = false
}

variable "iam_policy_export_json" {
  description = "Whether or not to export IAM policies as JSON."
  type        = bool
  default     = false
  nullable    = false
}

variable "iam_policy_export_actions" {
  description = "Whether or not to export IAM policies actions. A lightweight way to generate your own policies."
  type        = bool
  default     = false
  nullable    = false
}

variable "iam_policy_sid_prefix" {
  description = "Use a prefix for all `Sid` of all the policies created by this module."
  type        = string
  default     = ""
  nullable    = false
}

variable "iam_policy_source_arns" {
  description = "Restrict access to the key to the given ARN sources. Wildcards are allowed. Allowed keys are `ro`, `rw`, `rwd` or `full`, each specifying the expected level of access for entities inside."
  type        = map(map(string))
  nullable    = false
  default     = {}

  validation {
    condition = ((!contains([
      for key, arns in var.iam_policy_source_arns :
      (
        contains(["ro", "rw", "rwd", "audit", "full"], key) &&
        (
          !contains([
            for arn in values(arns) :
            (
              arn == null || can(regex("^arn:aws(-us-gov|-cn)?:[a-z]+:(([a-z]{2}-[a-z]{4,10}-[1-9]{1})?|\\*):([0-9]{12}|aws|\\*):.+/[a-zA-Z0-9+=,_:\\*\\./@{}'\\$\\?-]+$", arn))
            )
          ], false)
        )
      )
    ], false)))
    error_message = "One or more “var.iam_policy_source_arns” are invalid."
  }
}

variable "iam_policy_entity_arns" {
  description = "Restrict access to the key to the given IAM entities (roles or users). Wildcards are allowed. Allowed keys are `ro`, `rw`, `rwd`, `audit` or `full`, each specifying the permission scope for entities inside them. If the same entity is inside multiple scope, the least privileged scope takes precedence over the most privileged."
  type        = map(map(string))
  nullable    = false
  default     = {}

  validation {
    condition = (!contains([
      for key, arns in var.iam_policy_entity_arns :
      (
        contains(["ro", "rw", "rwd", "audit", "full"], key) &&
        (
          !contains([
            for arn in values(arns) :
            (
              arn == null || can(regex("^arn:aws(-us-gov|-cn)?:(iam|sts):(([a-z]{2}-[a-z]{4,10}-[1-9]{1})?|\\*):([0-9]{12}|aws|\\*):(assumed-role|role|user|federated-user)/[a-zA-Z0-9+=,_:\\*\\./@{}'\\$\\?-]+$", arn))
            )
          ], false)
        )
      )
    ], false))
    error_message = "One or more “var.iam_policy_entity_arns” are invalid."
  }
}

variable "iam_policy_restrict_by_account_ids" {
  description = "Restrict resources created by this module by the given account IDs. Internal policies will whitelist given list. If `null`, no restriction will be applied. Especially useful when IAM entities contains accounts wildcards."
  type        = list(string)
  default     = []
  nullable    = false

  validation {
    condition = ((!contains([
      for id in var.iam_policy_restrict_by_account_ids :
      (
        (can(regex("^[0-9]{12}$", id)))
      )
    ], false)))
    error_message = "One or more “var.iam_policy_restrict_by_account_ids” are invalid."
  }
}

variable "iam_policy_restrict_by_regions" {
  description = "Restrict resources created by this module by the given regions. Internal policies will whitelist given list. If `null`, no restriction will be applied. Especially useful when IAM entities contains region wildcards."
  type        = list(string)
  default     = []
  nullable    = false

  validation {
    condition = ((!contains([
      for id in var.iam_policy_restrict_by_regions :
      (
        (can(regex("^[a-z]{2}-[a-z]{4,10}-[1-9]{1}$", id)))
      )
    ], false)))
    error_message = "One or more “var.iam_policy_restrict_by_regions” are invalid."
  }
}

variable "iam_policy_identity" {
  description = <<-DOCUMENTATION
Identity-based IAM Policies to create.
Only policies fitting resource-based user-provided scopes (`ro`, `rw` and `full`) will be created.
    - name_template           (required, string):           Name template of the IAM Policies. Expect a `%s` to be replace with the user-provided scope (`ro`, `rw` and `full`).
    - description_template    (required, string):           Description template of the IAM Policies. Expect a `%s` to be replace with the user-provided scope (`ro`, `rw` and `full`).
    - path                    (optional, string, "/"):      Path of the IAM Policies.
    - tags                    (optional, map(string), {}):  Tags of the IAM Policies. Will be merged with `var.tags`.
DOCUMENTATION
  type = object({
    name_template        = string
    description_template = string
    path                 = optional(string, "/")
    tags                 = optional(map(string), {})
  })
  default = null
}

variable "identity_iam_policy_document" {
  description = <<-DOCUMENTATION
Information specific for the identity-based Policy documents.
Mandatory if `var.identity_iam_policy`, but might be used solely in combination with `var.identity_export_jsons`.
If entities were provided (through `var.entity_arns` or `var.source_arns`), only identity-based policies documents matching the scopes will be generated.
Otherwise, one policy document per user-provided scopes (keys of `var.scoped_actions`) will be generated.
  - source_policy_documents        (optional, list(string), []):      List of JSON strings to be concatenated to the polices. The same as `var.source_policy_documents` but exclusive to identity-based policies.
  - scoped_source_policy_documents (optional, map(list(string)), []): List of JSON strings to be concatenated to the polices, but scoped.
  - resources_arns                 (optional(list(string), []):       ARNs of resources created outside this module to compose the identity-based IAM Policies with. The same as `var.resources_arns` but exclusive to identity-based policies.
DOCUMENTATION
  type = object({
    source_policy_documents        = optional(list(string), [])
    scoped_source_policy_documents = optional(map(list(string)), {})
    resources_arns                 = optional(list(string), [])
  })
  default = null
}

#####
# IAM policy
#####

locals {
  iam_ro_actions = [
    "s3:GetObject*",
    "s3:ListBucket",
  ]
  iam_rw_actions = concat(
    local.iam_ro_actions,
    [
      "s3:PutObject*",
      "s3:ReplicateObject",
      "s3:RestoreObject",
    ]
  )
  iam_rwd_actions = concat(
    local.iam_rw_actions,
    [
      "s3:DeleteObject*",
    ]
  )
  iam_audit_actions = [
    "s3:GetAccelerateConfiguration",
    "s3:GetAnalyticsConfiguration",
    "s3:GetBucketAcl",
    "s3:GetBucketCORS",
    "s3:GetBucketCORS",
    "s3:GetBucketLogging",
    "s3:GetBucketNotification",
    "s3:GetBucketObjectLockConfiguration",
    "s3:GetBucketOwnershipControls",
    "s3:GetBucketPolicy",
    "s3:GetBucketPolicyStatus",
    "s3:GetBucketPublicAccessBlock",
    "s3:GetBucketRequestPayment",
    "s3:GetBucketRequestPayment",
    "s3:GetBucketTagging",
    "s3:GetBucketVersioning",
    "s3:GetBucketWebsite",
    "s3:GetEncryptionConfiguration",
    "s3:GetIntelligentTieringConfiguration",
    "s3:GetInventoryConfiguration",
    "s3:GetLifecycleConfiguration",
    "s3:GetReplicationConfiguration",
  ]
  iam_full_actions = [
    "s3:*",
  ]

  iam_scoped_actions = {
    ro    = local.iam_ro_actions
    rw    = local.iam_rw_actions
    rwd   = local.iam_rwd_actions
    audit = local.iam_audit_actions
    full  = local.iam_full_actions
  }

  identity_iam_policy_document = merge(
    {
      iam_policy_source_policy_documents = var.iam_policy_source_policy_documents
      resources_arns = flatten(concat(
        try(var.identity_iam_policy_document.resources_arns, []),
        concat([
          for k, v in aws_s3_bucket.this : [
            v.arn,
            "${v.arn}/*"
          ]
        ])
      ))
    },
    local.kms_should_create ? {
      scoped_source_policy_documents = {
        for scope, doc in module.kms["0"].precomputed.aws_iam_policies :
        scope => [doc.json]
      }
    } : {}
  )
}

data "aws_iam_policy_document" "tls" {
  for_each = var.buckets_require_secure_transport ? local.buckets_with_policy : {}

  statement {
    actions = local.iam_full_actions
    condition {
      test     = "Bool"
      values   = ["false"]
      variable = "aws:SecureTransport"
    }
    effect = "Deny"
    resources = [
      "arn:aws:s3:::${var.testing_prefix}${each.value.name}",
      "arn:aws:s3:::${var.testing_prefix}${each.value.name}/*",
    ]
    sid = "${var.iam_policy_sid_prefix}RequireSecureTransport"
    principals {
      identifiers = ["*"]
      type        = "AWS"
    }
  }
}

module "iam_policy_resource_based" {
  source  = "gitlab.com/wild-beavers/module-aws-iam-policy/aws"
  version = "~> 1"

  for_each = local.buckets_with_policy

  current_account_id = var.current_account_id
  testing_prefix     = var.testing_prefix
  tags               = local.tags

  entity_arns = var.iam_policy_entity_arns
  resource_arns = [
    "arn:aws:s3:::${var.testing_prefix}${each.value.name}",
    "arn:aws:s3:::${var.testing_prefix}${each.value.name}/*",
  ]
  scoped_actions          = local.iam_scoped_actions
  sid_prefix              = var.iam_policy_sid_prefix
  source_arns             = var.iam_policy_source_arns
  restrict_by_account_ids = var.iam_policy_restrict_by_account_ids
  source_policy_documents = concat(
    var.buckets_require_secure_transport ? [data.aws_iam_policy_document.tls[each.key].json] : [],
    var.iam_policy_source_policy_documents,
  )
}

module "iam_policy_identity_based" {
  source  = "gitlab.com/wild-beavers/module-aws-iam-policy/aws"
  version = "~> 1"

  for_each = var.iam_policy_identity != null || var.iam_policy_export_json || var.iam_policy_export_actions ? { 0 = 0 } : {}

  current_account_id = var.current_account_id
  testing_prefix     = var.testing_prefix
  tags               = local.tags

  scoped_actions = local.iam_scoped_actions
  sid_prefix     = var.iam_policy_sid_prefix

  identity_iam_policy          = var.iam_policy_identity
  identity_iam_policy_document = local.identity_iam_policy_document
  identity_export_jsons        = var.iam_policy_export_json
}

output "aws_iam_policies" {
  value = var.iam_policy_identity != null || var.iam_policy_export_json ? module.iam_policy_identity_based["0"].aws_iam_policies : null
}
